#include "fusedspheres/core/interface.hpp"
#include "fusedspheres/core/algorithm.hpp"

namespace fusedspheres {
// double Interface::arc_length() const {
//     double l = segments.size() > 0 ? 0 : 2 * M_PI;

//     for (auto& seg : segments) {
//         l += seg.is_arc() * seg.phi;
//     }
//     l *= R;

//     return l;
// }

bool Face::truncate(Vec3d r1, Vec3d n1) {
    r1 -= r; // transfrom to local coordinate system
    double a = n.dot(n1);
    double b = n.dot(r1);

    /* handle co/antiplanarity */
    if (a > 1 - eps) {
        return b > 0; // coplanar
    } else if (a < -1 + eps) {
        return b < 0; // antiplanar
    }

    /* calculate intersection line and project onto f */
    double u = n1.dot(r1) / sqrt(1 - a * a);
    return ArcPoly::truncate((P * n1).normalized(), u);
}

double Face::omega(size_t i) const {
    double Rs = sqrt(R * R + v[i] * v[i]); // sphere radius

    /* negative v will yield negative omega */
    if (segments.empty())
        return 2 * M_PI * (1 - v[i] / Rs - 2 * (v[i] < 0));

    double omg = 0;
    double s = sign(v[i]);

    /* add lune contributions */
    int N = 0;
    for (auto& seg : segments) {
        if (seg.is_arc())
            omg += s * solid_angle_lune(seg.phi, s * v[i] / Rs);

        ++N;
    }
    /* add triangle contribution if there are any */
    if (N > 2) {
        /* calculate normal vectors to corners of the triangle */
        std::vector<Vec3d> normals;
        normals.reserve(N);
        for (auto& seg : segments) {
            normals.push_back({seg.p[0], seg.p[1], v[i]});
            normals.back().normalize();
        }

        /* add solid angle of tetrahedron */
        auto& a = normals[0];
        for (int i = 1; i < N - 1; ++i) {
            auto& b = normals[i];
            auto& c = normals[i + 1];
            omg += solid_angle_tetrahedron(a, b, c);
        }
    }
    return omg;
}

// void Interface::populate() {
//     populate_angles_and_area();
//     omega[0] = calculate_omega(v[0]);
//     omega[1] = calculate_omega(v[1]);
// }

// void Interface::populate_angles_and_area() {
//     int N = segments.size();
//     if (N == 0) {
//         area = M_PI * R * R;
//         return;
//     }

//     area = 0;
//     for (int i = 0; i < N; ++i) {
//         auto& seg = segments[i];
//         auto& next = segments[(i + 1) % N];
//         double y = seg.p[0] * next.p[1] - seg.p[1] * next.p[0];
//         if (seg.is_arc()) {
//             seg.phi = atan2(y, seg.p.dot(next.p)) + (y < 0) * 2 * M_PI;
//             area += 0.5 * seg.phi * R * R;
//         } else {
//             area += 0.5 * y;
//         }
//     }
// }

// void Interface::set_coordinate_system(Vec3 d) {
//     d = n.cross(d); // y direction
//     d.normalize();
//     P.row(1) = d;
//     P.row(0) = P.row(1).cross(n);
//     has_coords = true;
// }

// Interface::TruncationType Interface::truncate(Vec3 r1, Vec3 n1) {
//     r1 -= r; // transfrom to local coordinate system
//     double a = n.dot(n1);
//     double b = n.dot(r1);

//     /* handle co/antiplanarity */
//     bool coplanar = a > 1 - eps;
//     bool antiplanar = a < -1 + eps;
//     if (coplanar && b > 0)
//         return TruncationType::None;
//     else if (coplanar && b <= 0)
//         return TruncationType::Full;
//     else if (antiplanar && b < 0)
//         return TruncationType::None;
//     else if (antiplanar && b >= 0)
//         return TruncationType::Full;

//     /* calculate intersection line and project onto f */
//     double u = n1.dot(r1) / sqrt(1 - a * a);

//     /* handle full or no truncation */
//     if (u > R)
//         return TruncationType::None;
//     else if (u < -R)
//         return TruncationType::Full;

//     if (!has_coords) set_coordinate_system(n1);

//     Vec2 x = P * n1; // 2d support vector
//     x.normalize();
//     Vec2 d{-x[1], x[0]}; // direction vector
//     x *= u;
//     double c = sqrt(R * R - u * u);
//     Vec2 p1 = x - c * d; // entry point on disk
//     Vec2 p2 = x + c * d; // exit point on disk

//     if (segments.size() == 0) {
//         segments.push_back({p1, -1, d, u});
//         segments.push_back({p2, 1});
//         return TruncationType::Partial;
//     }

//     int N = segments.size();

//     std::vector<bool> is_member(N);
//     bool all_member = true;
//     bool any_member = false;
//     for (int i = 0; i < N; ++i) {
//         auto& seg = segments[i];
//         is_member[i] = d[1] * seg.p[0] - d[0] * seg.p[1] < u;
//         all_member &= is_member[i];
//         any_member |= is_member[i];

//         /* check for parallel if straight */
//         if (seg.is_straight()) {
//             if (is_identical(d, seg.d) && u > seg.u - eps)
//                 return TruncationType::None;
//             else if (is_identical(d, -seg.d) && u < -seg.u + eps)
//                 return TruncationType::Full;
//         }
//     }

//     /* line intersects an arc or passes without intersection */
//     if (all_member || !any_member) {
//         int i1 = find_arc_intersection(segments, p1);
//         int i1next = (i1 + 1) % N;
//         /* if intersection could be found */
//         if (i1 < N) {
//             /* remainder of the arcpolygon is a D shaped region */
//             if (!any_member) {
//                 splice(segments, 0, N, {p1, -1, d, u}, {p2, 1});
//             }
//             /* only arc i1 gets truncated */
//             else if (all_member) {
//                 splice(segments, i1next, i1next, {p1, -1, d, u}, {p2, 1});
//             }
//             return TruncationType::Partial;
//         } else if (all_member)
//             return TruncationType::None;
//         else
//             return TruncationType::Full;
//     }

//     /* Line crosses between points*/
//     /* find the intersected segments along the line (d, u) */
//     auto [i1, i2] = find_intersections(is_member);

//     auto& seg1 = segments[i1];
//     auto& a1 = seg1.p;
//     int i1next = (i1 + 1) % N;
//     auto& a2 = segments[i1next].p;
//     if (seg1.is_straight()) p1 = line_intersection(p1, p2, a1, a2);

//     auto& seg2 = segments[i2];
//     auto& b1 = seg2.p;
//     int i2next = (i2 + 1) % N;
//     auto& b2 = segments[i2next].p;
//     if (seg2.is_straight()) p2 = line_intersection(p1, p2, b1, b2);

//     splice(segments, i1next, i2next, {p1, -1, d, u},
//            {p2, seg2.phi, seg2.d, seg2.u});

//     return TruncationType::Partial;
// }
} // namespace fusedspheres
