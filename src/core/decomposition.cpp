#include "fusedspheres/core/decomposition.hpp"
#include <algorithm>
#include <omp.h>
#include <optional>

namespace fusedspheres {
/**
 * Create a new Face object for s1 -> s2
 * @param  s1               Sphere 1
 * @param  s2               Sphere 2
 * @return    Face from s1 to s2
 */
Face create_interface(const Sphere& s1, const Sphere& s2) {
    auto f = Face(s1.id, s2.id);
    f.n = s2.r - s1.r;
    double d = f.n.norm();
    f.n *= 1. / d;
    f.P = generate_projection(f.n);

    double a = (s1.R * s1.R - s2.R * s2.R) / d;
    f.v[0] = (d + a) / 2;
    f.v[1] = (d - a) / 2;
    f.R = sqrt(s1.R * s1.R - f.v[0] * f.v[0]);
    f.r = s1.r + f.v[0] * f.n;

    return f;
}

SphereIntersection intersection_type(const Sphere& a, const Sphere& b) {
    /* allow reinsertion but delete previously inserted */
    if (a.id == b.id)
        return SphereIntersection::SecondIncluded;

    double d2 = (a.r - b.r).squaredNorm();
    double d2max = (a.R + b.R);
    d2max *= d2max;

    if (d2 < d2max) {
        // double d = sqrt(d2);
        double c = a.R - b.R;
        c *= c;

        if (d2 < c && a.R < b.R)
            return SphereIntersection::FirstIncluded;
        else if (d2 < c && b.R < a.R)
            return SphereIntersection::SecondIncluded;
        else
            return SphereIntersection::Regular;
    }
    return SphereIntersection::None;
}

/**
 * @brief Find all spheres in map that are included in or intersect s
 *
 * @param spheres map of spheres
 * @param s sphere for which to check intersection
 * @return {s is included in another, included in s, intersecting sC}
 */
std::tuple<bool, IdVec, IdVec>
find_included_and_intersected_spheres(const SphereMap& spheres,
                                      const Sphere& s) {
    IdVec included, intersected;

    for (auto& [j, o] : spheres) {
        switch (intersection_type(s, o)) {
        case SphereIntersection::None:
            break;
        case SphereIntersection::FirstIncluded:
            return {true, {}, {}};
        case SphereIntersection::SecondIncluded:
            included.push_back(j);
            break;
        case SphereIntersection::Regular:
            intersected.push_back(j);
            break;
        }
    }

    return {false, included, intersected};
}

/**
 * @brief Find all spheres in map that are included in or intersect s
 *
 * @param spheres map of spheres
 * @param s sphere for which to check intersection
 * @param oct octree helping to find possible neighbours
 * @return {s is included in another, included in s, intersecting sC}
 */
std::tuple<bool, IdVec, IdVec>
find_included_and_intersected_spheres(const SphereMap& spheres, const Sphere& s,
                                      auto& oct) {
    /* create temporary vector of candidates for intersections */
    IdVec candidates;
    for (const auto& js : oct.traverse(s.r, s.R)) {
        candidates.insert(candidates.end(), js.begin(), js.end());
    }
    auto n = candidates.size();

    /* determine intersection types */
    std::vector<SphereIntersection> intersection_types(n);
#pragma omp parallel for
    for (size_t k = 0; k < n; ++k) {
        auto j = candidates[k];
        const auto& o = spheres.at(j);
        intersection_types[k] = intersection_type(s, o);
    }

    IdVec included, intersected;
    for (size_t k = 0; k < n; ++k) {
        auto j = candidates[k];
        switch (intersection_types[k]) {
        case SphereIntersection::None:
            break;
        case SphereIntersection::FirstIncluded:
            return {true, {}, {}};
        case SphereIntersection::SecondIncluded:
            included.push_back(j);
            break;
        case SphereIntersection::Regular:
            intersected.push_back(j);
            break;
        }
    }

    return {false, included, intersected};
}

/**
 * Construct the truncated interfaces of sphere s
 * @param s            Sphere to create interfaces for
 * @param spheres      Map of spheres
 * @param intersected  Set of keys of spheres that intersect s
 * @return bool, whether to discard s, and list of interfaces
 */
std::pair<bool, std::vector<Face>>
construct_truncated_sphere(const Sphere& s, const SphereMap& spheres,
                           const IdVec& intersected) {
    std::vector<Face> fs;
    fs.reserve(intersected.size());

    for (auto j : intersected) {
        auto f = create_interface(spheres.at(j), s);

        bool keep = true;
        for (const auto& g : fs) {
            if (!f.truncate(g.r, -g.n)) {
                keep = false;
                break;
            }
        }

        for (auto it = fs.begin(); it != fs.end();) {
            auto& g = *it;
            auto is_empty = !g.truncate(f.r, -f.n);

            /* if a fully truncated interface fully truncates another
             * interface, no interfaces remain */
            if (!keep && is_empty)
                return {true, {}};

            if (is_empty)
                it = fs.erase(it);
            else
                ++it;
        }

        if (keep)
            fs.push_back(f);
    }

    return {fs.size() == 0 && intersected.size() > 0, fs};
}

SingleVAS decompose_single(const SphereMap& spheres, int i) {
    SingleVAS result;
    auto s = spheres.at(i);
    auto [is_included, included, intersected] =
        find_included_and_intersected_spheres(spheres, s);

    if (is_included)
        return result;

    /* new interfaces always point towards s, so that f.s[1] == s.id */
    auto [is_fully_truncated, interfaces] =
        construct_truncated_sphere(s, spheres, intersected);

    if (is_fully_truncated)
        return result;

    bool missing_center = false;
    for (auto& f : interfaces) {
        f.populate();
        result.V += f.area * f.v[1] / 3;
        result.A -= f.omega(1);
        result.S.emplace_back(f.id, f.area);
        missing_center |= f.v[1] < 0;
    }

    if (!missing_center)
        result.A += 4 * M_PI;
    result.A *= s.R * s.R;
    result.V += result.A * s.R / 3;

    return result;
}

/**
 * @brief Get triplets for constructing sparse jacobian matrices
 *
 * @param spheres Map of spheres
 * @param i sphere id of which derivatives will be calculated
 * @param face_rows map of face index to row (should only contain necessary
 * faces)
 * @param h derivation step size
 * @param triplets output
 */
void derive_single(SphereMap spheres, int i, IdMap face_rows, double h,
                   Triplets& triplets) {
    auto ii = spheres.at(i).row;
    for (auto& [j, o] : spheres) {
        auto jj = o.row;
        for (int d = 0; d < 4; ++d) {
            /* select x, y, z, or R for displacement */
            auto& q = d < 3 ? o.r[d] : o.R;

            /* displace forwards by h/2 */
            q += h / 2;
            auto fwd = decompose_single(spheres, i);
            /* displace backwards by h/2 */
            q -= h;
            auto bwd = decompose_single(spheres, i);
            /* undo displacement */
            q += h / 2;

            triplets.V[d].emplace_back(ii, jj, fwd.V - bwd.V);
            triplets.A[d].emplace_back(ii, jj, fwd.A - bwd.A);
            for (auto [l, S] : fwd.S) {
                auto it = face_rows.find(l);
                /* only add if interface exists in original deco and
                 * interface point from i to j */
                if (it != face_rows.end()) {
                    triplets.S[d].emplace_back(it->second, jj, S);
                }
            }
            for (auto [l, S] : bwd.S) {
                auto it = face_rows.find(l);
                /* only add if interface exists in original deco and
                 * interface point from i to j */
                if (it != face_rows.end()) {
                    triplets.S[d].emplace_back(it->second, jj, -S);
                }
            }
        }
    }
}

bool Decomposition::insert_sphere(Sphere s) {
    bool is_included;
    IdVec included, intersected;
    if (use_oct_)
        std::tie(is_included, included, intersected) =
            find_included_and_intersected_spheres(spheres, s, oct_);
    else
        std::tie(is_included, included, intersected) =
            find_included_and_intersected_spheres(spheres, s);

    if (is_included)
        return false;

    /* delete spheres that are included in new sphere */
    for (auto id : included)
        delete_sphere(id);

    /* create fully truncated interfaces from intersected spheres to s */
    auto [is_fully_truncated, new_interfaces] =
        construct_truncated_sphere(s, spheres, intersected);

    if (is_fully_truncated)
        return false;

    /* truncate interfaces of adjacent spheres */
    // std::vector<std::array<int, 2>> deleted_pairs;
    for (auto& f : new_interfaces) {
        auto& o = spheres.at(f.s[0]); // other sphere is s[0] by convention
        auto faces_copy = o.faces; // copy faces because they might get modified
        for (auto l : faces_copy) {
            auto& g = faces.at(l);
            if (!g.truncate(f.r, f.n)) {
                // deleted_pairs.push_back(f.s);
                delete_face(l);
            }
        }
    }

    // auto delete_face_if_exists = [&](int i, int j) {
    //     auto it = faces.find(cantor(i, j));
    //     if (it == faces.end()) it = faces.find(cantor(i, j));

    // };
    // for (auto it1 = deleted_pairs.begin(); it1 != deleted_pairs.end(); ++it1)
    // {
    //     auto [a, b] = *it1;
    //     for (auto it2 = it1 + 1; it2 != deleted_pairs.end(); ++it2) {
    //         auto [c, d] = *it2;
    //         if (a == c)
    //     }
    // }

    /* add new sphere and interfaces */
    spheres[s.id] = s;
    if (use_oct_)
        oct_.get(s.r, s.R).push_back(s.id);

    for (const auto& f : new_interfaces) {
        spheres[f.s[0]].faces.insert(f.id);
        spheres[f.s[1]].faces.insert(f.id);
        faces[f.id] = f;
    }

    /* delete all spheres that have lost all interfaces */
    // TODO this fails if a part of the graph is separated from the remaining
    // graph. Then not all nodes of this separated graph have no neighbours
    for (auto j : intersected)
        if (spheres.at(j).faces.size() == 0)
            delete_sphere(j);

    return true;
}

IdSet Decomposition::neighbours(int i) const {
    IdSet neigh;
    for (auto l : spheres.at(i).faces) {
        auto& f = faces.at(l);
        neigh.insert(i == f.s[0] ? f.s[1] : f.s[0]);
    }
    return neigh;
}

/**
 * @brief Calculate number, volumes, surface areas and interface areas
 *
 * @return tuple of N, V, A, S
 */
FullGeo Decomposition::calculate_NVAS() {
    FullGeo result;
    std::array<size_t, 4> num = {spheres.size(), faces.size(), 0, 0};
    VecXd V = VecXd::Zero(num[0]);
    VecXd A = VecXd::Zero(num[0]);
    VecXd S = VecXd::Zero(num[1]);
    std::vector<bool> missing_center(num[0], false);

    for (auto& [l, f] : faces) {
        auto [nlines, ncorners] = f.num_lines_corners();
        num[2] += nlines;
        num[3] += ncorners;
        f.populate();
        S[f.row] = f.area;
        for (int i = 0; i < 2; ++i) {
            auto& s = spheres.at(f.s[i]);
            auto v = f.v[i];
            V[s.row] += f.area * v / 3;
            A[s.row] -= f.omega(i);
            missing_center[s.row] = missing_center[s.row] || (v < 0);
        }
    }

    for (auto& [id, s] : spheres) {
        auto i = s.row;
        if (!missing_center[i])
            A[i] += 4 * M_PI;
        A[i] *= s.R * s.R;
        V[i] += A[i] * s.R / 3;
    }

    num[2] /= 3;
    num[3] /= 4;

    return std::tuple{num, V, A, S};
}

void Decomposition::derive(Jacobians& J, double h) {
    auto N1 = spheres.size();
    auto N2 = faces.size();
    auto N3 = N2; // TODO

    /* resize and set zero matrices */
    for (int d = 0; d < 4; ++d) {
        J.V[d].resize(N1, N1);
        J.V[d].data().squeeze();
        J.V[d].reserve(N1 + 2 * N2);
        J.A[d].resize(N1, N1);
        J.A[d].data().squeeze();
        J.A[d].reserve(N1 + 2 * N2);
        J.S[d].resize(N2, N1);
        J.S[d].data().squeeze();
        J.S[d].reserve(2 * N2 + 3 * N3);
    }

    std::vector<int> ids;
    for (auto& [i, s] : spheres)
        ids.push_back(i);

    // clang-format off
    Triplets ts;
    #pragma omp parallel
    {
        Triplets ts_local;

        #pragma omp for schedule(dynamic, 10) nowait
        for (size_t n = 0; n < ids.size(); ++n)
        {
            auto cluster = local_cluster(ids[n]);
            auto face_rows = face_row_map(ids[n]);

            derive_single(cluster, ids[n], face_rows, h, ts_local);
        }

        #pragma omp critical
        for (int d = 0; d < 4; ++d)
        {
            ts.V[d].insert(ts.V[d].end(), ts_local.V[d].begin(), ts_local.V[d].end());
            ts.A[d].insert(ts.A[d].end(), ts_local.A[d].begin(), ts_local.A[d].end());
            ts.S[d].insert(ts.S[d].end(), ts_local.S[d].begin(), ts_local.S[d].end());
        }
    }
    // clang-format on

    /* divide by step size */
    for (int d = 0; d < 4; ++d) {
        J.V[d].setFromTriplets(ts.V[d].begin(), ts.V[d].end());
        J.V[d] /= h;
        J.A[d].setFromTriplets(ts.A[d].begin(), ts.A[d].end());
        J.A[d] /= h;
        J.S[d].setFromTriplets(ts.S[d].begin(), ts.S[d].end());
        J.S[d] /= h;
    }
}

/* PRIVATE MEMBERS ============================================ */

void Decomposition::delete_face(int id) {
    auto f = faces.find(id);
    assert(f != faces.end() && "Attempted to delete non existing face");
    for (auto i : f->second.s)
        spheres.at(i).faces.erase(id);
    faces.erase(f);
}

/**
 * Delete sphere and all its interfaces
 * @param id  Sphere id
 */
inline void Decomposition::delete_sphere(int i) {
    /* remove sphere from octree */
    if (use_oct_) {
        const auto& s = spheres.at(i);
        auto& ids = oct_.get(s.r, s.R);
        ids.erase(std::find(ids.begin(), ids.end(), i));
    }

    /* delete all interfaces of the sphere */
    for (auto l : spheres[i].faces) {
        auto& f = faces[l];
        auto j = f.s[0] == i ? f.s[1] : f.s[0];
        spheres[j].faces.erase(l);
        faces.erase(l);
    }

    /* then delete the sphere */
    spheres.erase(i);
}

/**
 * Enumerate spheres and interfaces contiguously
 */
void Decomposition::enumerate() {
    std::vector<int> indices;
    /* enumerate spheres and interfaces */
    for (auto& [i, s] : spheres)
        indices.push_back(i);

    std::sort(indices.begin(), indices.end());

    int cnt = 0;
    for (auto i : indices)
        spheres[i].row = cnt++;

    indices.clear();
    for (auto& [l, f] : faces)
        indices.push_back(l);

    std::sort(indices.begin(), indices.end());

    cnt = 0;
    for (auto l : indices)
        faces[l].row = cnt++;
}

SphereMap Decomposition::local_cluster(int i) const {
    auto const& s = spheres.at(i);
    auto cluster = select(spheres, neighbours(i));
    cluster.emplace(i, s);
    return cluster;
}

IdMap Decomposition::face_row_map(int i) const {
    auto const& s = spheres.at(i);
    IdMap face_rows;
    for (auto l : s.faces)
        face_rows.emplace(l, faces.at(l).row);

    return face_rows;
}

#ifndef NDEBUG
[[gnu::noinline]] Face& Decomposition::dbg_interface(int i, int j) {
    return interface(i, j);
}

[[gnu::noinline]] Sphere& Decomposition::dbg_sphere(int i) {
    return spheres.at(i);
}
#endif
} // namespace fusedspheres
