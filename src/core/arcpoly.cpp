#include "fusedspheres/core/arcpoly.hpp"
#include "fusedspheres/core/algorithm.hpp"
#include "fusedspheres/core/linalg.hpp"

namespace fusedspheres {

/**
 * @brief Truncate the arcpolygon by a halfspace
 *
 * @param n 2d normal vector pointing into the truncated region
 * @param u offset of truncation line to center
 * @return true if anything remains of the arcpoly, false if fully truncated
 */
bool ArcPoly::truncate(Vec2d n, double u) {
    /* full truncation if u < -R and no truncation if u > R */
    if (fabs(u) > R)
        return u > 0;

    /* calculate intersections with circle */
    double a = sqrt(R * R - u * u);
    Vec2d d{-n[1], n[0]};
    Vec2d p1 = u * n - a * d;
    Vec2d p2 = u * n + a * d;

    /* if arcpolygon has not been truncated */
    if (segments.empty()) {
        segments.push_front({p1, d, u, -1});
        segments.push_front({p2, -d, u, 1});
        return true;
    }

    /* check for existing segments that (anti-)parallel to new seg */
    for (const auto& seg : segments) {
        if (seg.is_line()) {
            if (is_identical(d, seg.d) && u > seg.u - eps)
                return true;
            else if (is_identical(d, -seg.d) && u < -seg.u + eps)
                return false;
        }
    }

    auto [t, in, out] = detail::find_intersected_segments(segments, p1, p2);

    switch (t) {
    case detail::APTrunc::Regular:
        // cyclic_erase_between(segments, in, out);
        segments.cyclic_erase_between(in, out);
        if (in->is_line())
            p1 = line_intersection(p1, p2, in->p, in->p + in->d);
        segments.insert_after(in, {p1, d, u, -1});
        if (out->is_line())
            p2 = line_intersection(p1, p2, out->p, out->p + out->d);
        out->p = p2;
        return true;
    case detail::APTrunc::Empty:
        segments.clear();
        return false;
    case detail::APTrunc::None:
        return true;
    case detail::APTrunc::CircSeg:
        segments.clear();
        segments.push_front({p1, d, u, -1});
        segments.push_front({p2, -d, u, 1});
        return true;
    case detail::APTrunc::ArcTrunc:
        // reverse insertion order because of 'insert_after'
        segments.insert_after(in, {p2, -d, u, 1});
        segments.insert_after(in, {p1, d, u, -1});
        return true;
    }

    return true;

    // /* find points that are member of the */
    // std::vector<bool> is_member;
    // for (const auto& seg : segments)
    //     is_member.push_back(d[1] * seg.p[0] - d[0] * seg.p[1] < u);

    // auto all_member = std::all_of(std::begin(is_member), std::end(is_member),
    //                               [](auto v) { return v; });
    // auto any_member = std::any_of(std::begin(is_member), std::end(is_member),
    //                               [](auto v) { return v; });

    // auto N = segments.size();
    // /* line intersects an arc or passes without intersection */
    // if (all_member || !any_member) {
    //     auto i1 = detail::find_arc_intersection(segments, p1);
    //     auto i1next = (i1 + 1) % N;
    //     /* if intersection could be found */
    //     if (i1 < N) {
    //         /* remainder of the arcpolygon is a D shaped region */
    //         if (!any_member)
    //             splice(segments, 0, N, Seg{p1, d, u, -1},
    //                    Seg{p2, Vec2d::Constant(NAN), NAN, 1});
    //         /* only arc i1 gets truncated */
    //         else if (all_member) {
    //             splice(segments, i1next, i1next, Seg{p1, d, u, -1},
    //                    Seg{p2, Vec2d::Constant(NAN), NAN, 1});
    //         }

    //         return true;
    //     } else if (all_member)
    //         return true;
    //     else
    //         return false;
    // }

    // /* Line crosses between points*/
    // /* find the intersected segments along the line (d, u) */
    // auto [i1, i2] = detail::find_intersections(is_member);

    // auto& seg1 = segments[i1];
    // auto& a1 = seg1.p;
    // int i1next = (i1 + 1) % N;
    // auto& a2 = segments[i1next].p;

    // if (seg1.is_line())
    //     p1 = line_intersection(p1, p2, a1, a2);

    // auto& seg2 = segments[i2];
    // auto& b1 = seg2.p;
    // int i2next = (i2 + 1) % N;
    // auto& b2 = segments[i2next].p;

    // if (seg2.is_line())
    //     p2 = line_intersection(p1, p2, b1, b2);

    // splice(segments, i1next, i2next, Seg{p1, d, u, -1},
    //        Seg{p2, seg2.d, seg2.u, seg2.phi});

    // return true;
}

/**
 * @brief Calculate area of arcpolygon (expensive!)
 *
 * @return area
 */
void ArcPoly::populate() {
    if (segments.empty()) {
        area = M_PI * R * R;
        return;
    }

    area = 0;
    for (auto curr = segments.begin(); curr != segments.end(); ++curr) {
        auto next = cyclic_next(segments, curr);
        double y = curr->p[0] * next->p[1] - curr->p[1] * next->p[0];
        if (curr->is_arc()) {
            curr->phi = atan2(y, curr->p.dot(next->p));
            /* on arcs, map values in [-pi, 0) to [pi, 2*pi) */
            curr->phi += std::signbit(curr->phi) * 2 * M_PI;
            area += 0.5 * curr->phi * R * R;
        } else {
            area += 0.5 * y;
        }
    }
}

std::pair<int, int> ArcPoly::num_lines_corners() const {
    int nlines = 0;
    int ncorners = 0;
    for (auto curr = segments.begin(); curr != segments.end(); ++curr) {
        auto next = cyclic_next(segments, curr);
        nlines += curr->is_line();
        ncorners += curr->is_line() && next->is_line();
    }
    return std::pair{nlines, ncorners};
}

double ArcPoly::arc_length() const {
    double l = segments.empty() ? 2 * M_PI : 0;

    for (auto& seg : segments) {
        l += seg.is_arc() * seg.phi;
    }
    l *= R;

    return l;
}

double ArcPoly::circumference() const {
    double c = segments.empty() ? 2 * M_PI * R : 0;
    for (auto curr = segments.begin(); curr != segments.end(); ++curr) {
        if (curr->is_arc()) {
            c += R * curr->phi;
        } else {
            auto next = cyclic_next(segments, curr);
            c += (next->p - curr->p).norm();
        }
    }

    return c;
}
} // namespace fusedspheres
