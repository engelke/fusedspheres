#include "fusedspheres/mesh/wavefront.hpp"
#include <fmt/core.h>

namespace fusedspheres::wavefront {
void write_obj(FILE* file, std::string name, const mesh::Mesh& m, int n) {
    fmt::print(file, "o {}\n", name);
    for (auto x : m.vertices) {
        x += m.origin;
        fmt::print(file, "v {} {} {}\n", x[0], x[1], x[2]);
    }

    Map<int, Set<int>> groups;
    for (size_t k = 0; k < m.groups.size(); ++k)
        groups[m.groups[k]].insert(k);

    for (auto& [i, g] : groups) {
        fmt::print(file, "g {}_g{}\n", name, i);
        // enable or disable smooth shading for this group
        if (m.smooth.count(i))
            fmt::print(file, "s 1\n");
        else
            fmt::print(file, "s off\n");

        for (auto& k : g) {
            auto& f = m.faces[k];
            fmt::print(file, "f {} {} {}\n", f[0] + n, f[1] + n, f[2] + n);
        }
    }
}

void write_mtl(FILE* file, std::string name, const cmap::Colour& c) {
    fmt::print(file, "newmtl {}\n", name);
    fmt::print(file, "Kd {} {} {}\n", c.diffuse[0], c.diffuse[1], c.diffuse[2]);
    fmt::print(file, "Ka {} {} {}\n", c.ambient[0], c.ambient[1], c.ambient[2]);
    fmt::print(file, "Ks {} {} {}\n", c.specular[0], c.specular[1],
               c.specular[2]);
    fmt::print(file, "Ns {}\n", c.ns);
    fmt::print(file, "d {}\n", c.opacity);
}
} // namespace fusedspheres::wavefront
