#include "fusedspheres/mesh/mesh.hpp"
#include <fmt/core.h>
#include <map>
#include <set>

namespace {
using fusedspheres::Vec3d;
Vec3d penetration_point(const Vec3d& n, double v, const Vec3d& a,
                        const Vec3d& b) {
    double an = a.dot(n);
    double bn = b.dot(n);
    double s = (v - an) / (bn - an);
    return (1 - s) * a + s * b;
}
} // namespace

namespace fusedspheres::mesh {
Mesh create_icosphere(Vec3d origin, double radius, int subdivisions) {
    /* initial icosahedron (hard coded) */
    static const double a = .525731112119133606;
    static const double b = .850650808352039932;
    static const std::vector<Vec3d> vertices{
        {-a, 0, b}, {a, 0, b},  {-a, 0, -b}, {a, 0, -b},
        {0, b, a},  {0, b, -a}, {0, -b, a},  {0, -b, -a},
        {b, a, 0},  {-b, a, 0}, {b, -a, 0},  {-b, -a, 0}};
    static const std::vector<Face> faces{
        {0, 4, 1},  {0, 9, 4},  {9, 5, 4},  {4, 5, 8},  {4, 8, 1},
        {8, 10, 1}, {8, 3, 10}, {5, 3, 8},  {5, 2, 3},  {2, 7, 3},
        {7, 10, 3}, {7, 6, 10}, {7, 11, 6}, {11, 0, 6}, {0, 1, 6},
        {6, 1, 10}, {9, 0, 11}, {9, 11, 2}, {9, 2, 5},  {7, 2, 11}};
    static const std::vector<int> groups(20, 0);

    Mesh m{origin, vertices, faces, groups, {0}};
    for (auto& v : m.vertices) {
        v *= radius / v.norm();
    }
    /* do subdivision and projections sphere */
    for (int i = 0; i < subdivisions; ++i) {
        refine_subdivide_edges(m);
        for (auto& v : m.vertices) {
            v *= radius / v.norm();
        }
    }

    double angle = Vec2d::Random()[0];
    Vec3d axis = Vec3d::Random();
    axis.normalize();
    Eigen::AngleAxis rot(angle, axis);
    for (auto& x : m.vertices)
        x = rot * x;

    return m;
}

void refine_subdivide_edges(Mesh& m) {
    auto faces = std::move(m.faces);
    auto groups = std::move(m.groups);
    m.faces.reserve(4 * faces.size());

    EdgeMap<Index> edge_vertex;
    for (size_t k = 0; k < faces.size(); ++k) {
        auto& f = faces[k];
        Face fc;
        for (Index i = 0; i < 3; ++i) {
            auto a = f[i];
            auto b = f[(i + 1) % 3];

            /* create new vertex on midpoint of edge if missing */
            if (!edge_vertex.count({a, b})) {
                edge_vertex[{a, b}] = m.vertices.size();
                m.vertices.push_back(0.5 * (m.vertices[a] + m.vertices[b]));
            }
            fc[i] = edge_vertex[{a, b}];
        }

        m.faces.push_back(fc);
        m.faces.push_back({f[0], fc[0], fc[2]});
        m.faces.push_back({f[1], fc[1], fc[0]});
        m.faces.push_back({f[2], fc[2], fc[1]});

        /* put all new faces in the same group as the original face */
        m.groups.insert(m.groups.end(), 4, groups[k]);
    }
}

void remove_unconnected(Mesh& m) {
    /* get all connected vertices */
    std::map<Index, Index> connected; // map is sorted
    for (auto& f : m.faces) {
        for (Index i = 0; i < 3; ++i)
            connected[f[i]] = 0;
    }

    /* enumerate */
    Index cnt = 0;
    for (auto& [i_old, i] : connected)
        i = cnt++;

    /* remove all unconnected */
    cnt = 0;
    auto last = std::remove_if(
        m.vertices.begin(), m.vertices.end(),
        [&connected, &cnt](const auto&) { return !connected.count(cnt++); });
    m.vertices.erase(last, m.vertices.end());

    for (auto& f : m.faces)
        for (auto& i : f)
            i = connected[i];
}

void transform_scale(Mesh& m, double factor) {
    for (auto& v : m.vertices)
        v *= factor;
}

void truncate(Mesh& m, Vec3d n, double v) {
    std::vector<bool> inside;
    for (auto& x : m.vertices) {
        inside.push_back(x.dot(n) < v);
    }

    EdgeMap<Index> penetration;
    auto create_or_get_penetration = [&](Index i, Index j) {
        if (!penetration.count({i, j})) {
            penetration[{i, j}] = m.vertices.size();
            m.vertices.push_back(
                penetration_point(n, v, m.vertices[i], m.vertices[j]));
        }
        return penetration[{i, j}];
    };

    auto faces = std::move(m.faces);
    auto groups = std::move(m.groups);
    std::vector<Face> new_faces;
    Set<Index> new_vertices;
    for (size_t k = 0; k < faces.size(); ++k) {
        auto& f = faces[k];
        auto g = groups[k];
        for (int i = 0; i < 3; ++i) {
            auto a = f[i];
            auto b = f[(i + 1) % 3];
            auto c = f[(i + 2) % 3];

            /* per face only one of the following options is executed */
            if (inside[a] && inside[b] && inside[c]) {
                m.faces.push_back(f);
                m.groups.push_back(g);
                break;
            } else if (inside[a] && inside[b] && !inside[c]) {
                auto d = create_or_get_penetration(a, c);
                auto e = create_or_get_penetration(b, c);
                m.faces.push_back({a, b, d});
                m.groups.push_back(g);
                m.faces.push_back({b, e, d});
                m.groups.push_back(g);

                new_faces.push_back({e, d, -1});
                new_vertices.insert(d);
                new_vertices.insert(e);
            } else if (inside[a] && !inside[b] && !inside[c]) {
                auto d = create_or_get_penetration(a, b);
                auto e = create_or_get_penetration(a, c);
                m.faces.push_back({a, d, e});
                m.groups.push_back(g);

                new_faces.push_back({d, e, -1});
                new_vertices.insert(d);
                new_vertices.insert(e);
            }
        }
    }

    /* calculate truncation face center */
    Vec3d r0 = Vec3d::Constant(0);
    for (auto i : new_vertices) {
        r0 += m.vertices[i];
    }
    r0 *= 1. / new_vertices.size();
    Index i_center = m.vertices.size();
    m.vertices.push_back(r0);

    /* insert new faces into mesh as a new group */
    auto max_group_it = std::max_element(m.groups.begin(), m.groups.end());
    auto new_group = max_group_it == m.groups.end() ? 0 : *max_group_it + 1;

    for (auto& f : new_faces) {
        f[2] = i_center;
        m.faces.push_back(f);
        m.groups.push_back(new_group);
    }

    remove_unconnected(m);
}

} // namespace fusedspheres::mesh
