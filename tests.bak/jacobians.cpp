#include "fusedspheres/decomposition.hpp"

#include <fmt/core.h>
#include <fmt/ostream.h>
#include <fmt/ranges.h>

const double eps = 1e-4;

bool is_equal(double a, double b) { return fabs(2. * (a - b) / (a + b)) < eps; }

bool is_equal(Eigen::MatrixXd a, Eigen::MatrixXd b)
{
    return 2 * (a - b).norm() / (a.norm() + b.norm()) < eps;
}

bool is_zero(Eigen::MatrixXd m) { return m.norm() < eps; }

bool is_symmetric(Eigen::MatrixXd m)
{
    return (m - m.transpose()).norm() / m.norm() < eps;
}

void test(std::string msg, bool success)
{
    fmt::print("{}: {}\n", msg,
               success ? "\033[1;32mOK\033[0m" : "\033[1;31mFAIL\033[0m");

    if (!success) exit(EXIT_FAILURE);
}

int main()
{
    using namespace fusedspheres;
    using Mat = Eigen::MatrixXd;

    {
        fmt::print("Jacobian Test 1: 2 Spheres Same Radii\n");
        Decomposition deco;
        deco.insert_sphere(0, Vec3d{0, 0, 0}, 1);
        deco.insert_sphere(1, Vec3d{1, 0, 0}, 1);
        deco.populate();

        Decomposition::Jacobians J;
        deco.derive(J);

        test("Value 0,0 J.V[x]", is_equal(J.V[0].coeff(0, 0), -3 * M_PI / 8.));
        test("Value 0,0 J.A[x]", is_equal(J.A[0].coeff(0, 0), -M_PI));
        test("Value 0,0 J.S[x]", is_equal(J.S[0].coeff(0, 0), M_PI / 2));

        test("Is zero J.V[y]", is_zero(J.V[1]));
        test("Is zero J.A[y]", is_zero(J.A[1]));
        test("Is zero J.S[y]", is_zero(J.S[1]));

        test("Is zero J.V[z]", is_zero(J.V[2]));
        test("Is zero J.A[z]", is_zero(J.A[2]));
        test("Is zero J.S[z]", is_zero(J.S[2]));

        test("Value 0,0 J.V[R]", is_equal(J.V[3].coeff(0, 0), 15 * M_PI / 4));
        test("Value 1,0 J.V[R]", is_equal(J.V[3].coeff(1, 0), -3 * M_PI / 4));
        test("Value 0,0 J.A[R]", is_equal(J.A[3].coeff(0, 0), 7 * M_PI));
        test("Value 1,0 J.A[R]", is_equal(J.A[3].coeff(1, 0), -2 * M_PI));
        test("Value 0,0 J.S[R]", is_equal(J.S[3].coeff(0, 0), M_PI));

        test("Symmetry J.V[R]", is_symmetric(J.V[3]));
        test("Symmetry J.A[R]", is_symmetric(J.A[3]));
        test("Symmetry J.S[R]",
             is_equal(J.S[3].coeff(0, 0), J.S[3].coeff(0, 1)));

        fmt::print("\n");
    }

    {
        fmt::print("Jacobian Test 2: 2 Spheres Diffferent Radii\n");
        Decomposition deco;
        deco.insert_sphere(0, Vec3d{0, 0, 0}, 2);
        deco.insert_sphere(1, Vec3d{2, 0, 0}, 1);
        deco.populate();

        Decomposition::Jacobians J;
        deco.derive(J);

        test("Values J.V[x]",
             is_equal(J.V[0], Mat{{-0.368155, 0.368155}, {-2.57709, 2.57709}}));

        test("Values J.A[x]",
             is_equal(J.A[0], Mat{{-1.5708, 1.5708}, {-5.49779, 5.49779}}));

        test("Values J.S[x]", is_equal(J.S[0], Mat{{1.37445, -1.37445}}));

        test("Is zero J.V[y]", is_zero(J.V[1]));
        test("Is zero J.A[y]", is_zero(J.A[1]));
        test("Is zero J.S[y]", is_zero(J.S[1]));

        test("Is zero J.V[z]", is_zero(J.V[2]));
        test("Is zero J.A[z]", is_zero(J.A[2]));
        test("Is zero J.S[z]", is_zero(J.S[2]));

        test("Values J.V[R]",
             is_equal(J.V[3], Mat{{50.0691, -1.47262}, {-2.94524, 9.3266}}));

        test("Values J.A[R]",
             is_equal(J.A[3], Mat{{48.6947, -6.28319}, {-6.28319, 17.2788}}));

        test("Values J.S[R]", is_equal(J.S[3], Mat{{1.5708, 5.49779}}));
    }
    // {
    //     fmt::print("Partial Evaluation 3: ");
    //     Decomposition deco;
    //     double u = 1. / sqrt(2);
    //     Vec3d a{u, u, 0}, b{0, u, u}, c{u, 0, u};
    //     deco.insert_sphere(0, {0, 0, 0}, 1);
    //     deco.insert_sphere(1, a, 1);
    //     deco.insert_sphere(2, b, 1);
    //     deco.insert_sphere(3, c, 1);
    //
    //     deco.populate();
    //
    //     Decomposition::Jacobians J;
    //     deco.derive(J);
    // }
}
