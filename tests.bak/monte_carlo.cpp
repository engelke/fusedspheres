#include "fusedspheres/decomposition.hpp"
#include "fusedspheres/random.hpp"

#include <fmt/core.h>
#include <fmt/ostream.h>
#include <fmt/ranges.h>

using namespace fusedspheres;

const Face& getf(const Decomposition& deco, int l) { return deco.faces.at(l); }
const Sphere& gets(const Decomposition& deco, int i) {
    return deco.spheres.at(i);
}

double sample_volume(const std::vector<Sphere>& spheres, int n,
                     unsigned seed = 0) {
    Vec3d x0{1e10, 1e10, 1e10};
    Vec3d l{0, 0, 0};
    for (auto& s : spheres) {
        for (int d = 0; d < 3; ++d) {
            x0[d] = std::min(x0[d], s.r[d] - s.R - 0.1);
            l[d] = std::max(l[d], s.r[d] + s.R + 0.1);
        }
    }
    l -= x0;

    RandomReal<double> rng(seed);

    int i = 0;
    for (int iter = 0; iter < n; ++iter) {
        Vec3d x{l[0] * rng(), l[1] * rng(), l[2] * rng()};
        x += x0;
        for (auto& s : spheres) {
            if ((s.r - x).squaredNorm() < s.R * s.R) {
                ++i;
                break;
            }
        }
    }

    return i * l[0] * l[1] * l[2] / n;
}

bool check(const std::vector<Sphere>& spheres, int niter, double eps) {
    Decomposition deco;
    for (auto& s : spheres)
        deco.insert_sphere(s);

    deco.populate();
    double Vd = 0;
    // fmt::print("{:>3} {:>10.7} {:>10.7} {:>10.7}\n", "id", "V", "A", "S");
    for (auto& [i, s] : deco.spheres) {
        Vd += s.V;
        // fmt::print("{:3} {:10.7} {:10.7} {:10.7} {}\n", s.id, s.V, s.A, s.S,
        // s.faces);
    }
    fmt::print("Analytical volume: {}\n", Vd);

    double Vm = sample_volume(spheres, niter);
    fmt::print("Monte Carlo volume: {:.7} ({} iterations)\n", Vm, niter);
    bool pass = fabs((Vd - Vm) / Vd) < eps;
    fmt::print("Pass: {}\n\n",
               pass ? "\033[1;32mOK\033[0m" : "\033[1;31mFAIL\033[0m");
    return pass;
}

int main() {
    {
        fmt::print("Monte Carlo Test 1: 3 Spheres\n");
        std::vector<Sphere> spheres;
        int id = 0;
        spheres.emplace_back(id++, Vec3d{0, 0, 0}, 1);
        spheres.emplace_back(id++, Vec3d{1, 0, 0}, 1);
        spheres.emplace_back(id++, Vec3d{.5, 1, 0}, 1);

        assert(check(spheres, 1e7, 2e-4));
    }
    {
        fmt::print("Monte Carlo Test 2: Cube of Spheres\n");
        std::vector<Sphere> spheres;
        int id = 0;
        spheres.emplace_back(id++, Vec3d{0, 0, 0}, 1);
        spheres.emplace_back(id++, Vec3d{1, 0, 0}, 1);
        spheres.emplace_back(id++, Vec3d{0, 1, 0}, 1);
        spheres.emplace_back(id++, Vec3d{1, 1, 0}, 1);
        spheres.emplace_back(id++, Vec3d{0, 0, 1}, 1);
        spheres.emplace_back(id++, Vec3d{1, 0, 1}, 1);
        spheres.emplace_back(id++, Vec3d{0, 1, 1}, 1);
        spheres.emplace_back(id++, Vec3d{1, 1, 1}, 1);

        assert(check(spheres, 1e7, 1e-4));
    }
    {
        fmt::print("Monte Carlo Test 2: Random\n");
        std::vector<Sphere> spheres;
        int N = 40;
        RandomReal<double> rng(1);
        double l = 5;
        for (int i = 0; i < N; ++i) {
            spheres.emplace_back(i, Vec3d{l * rng(), l * rng(), l * rng()}, 1);
        }

        assert(check(spheres, 1e7, 2e-4));
    }
}
