#include "fusedspheres/decomposition.hpp"
#include <cassert>
#include <fmt/ostream.h>
#include <fmt/ranges.h>

int main() {
    using namespace fusedspheres;

    {
        fmt::print("Testing sphere insertion\n");
        Decomposition deco;
        deco.insert_sphere(Sphere(0, {0, 0, 0}, 2));
        deco.insert_sphere(Sphere(1, {3, 0, 0}, 2));
        deco.insert_sphere(Sphere(2, {0, 0, 0}, 1));
        assert(deco.spheres.size() == 2);
        assert(deco.faces.size() == 1);

        deco.insert_sphere(Sphere(3, {6, 0, 0}, 2));
        assert(deco.spheres.size() == 3);
        assert(deco.faces.size() == 2);

        deco.insert_sphere(Sphere(4, {1.5, 2, 0}, 2));
        assert(deco.spheres.size() == 4);
        assert(deco.faces.size() == 4);

        deco.insert_sphere(Sphere(5, {6, 0, 0}, 3));
        assert(deco.spheres.size() == 4);
        assert(deco.faces.size() == 4);
    }
    {
        fmt::print("Testing tetrahedron\n");
        Decomposition deco;
        int id = 0;
        deco.insert_sphere(id++, {sqrt(3) / 2, -0.5, 0}, 2);
        deco.insert_sphere(id++, {0, 1, 0}, 2);
        deco.insert_sphere(id++, {-sqrt(3) / 2, -0.5, 0}, 2);
        deco.insert_sphere(id++, {0, 0, 1}, 2);

        assert(deco.faces.size() == 6);
        for (auto& [l, f] : deco.faces)
            assert(f.segments.size() == 3);
    }
    {
        fmt::print("Testing square pyramid\n");
        Decomposition deco;
        int id = 0;
        deco.insert_sphere(id++, {1, 1, 0}, 2);
        deco.insert_sphere(id++, {-1, 1, 0}, 2);
        deco.insert_sphere(id++, {-1, -1, 0}, 2);
        deco.insert_sphere(id++, {1, -1, 0}, 2);
        deco.insert_sphere(id++, {0, 0, 1}, 2);

        assert(deco.faces.size() == 8);
        for (auto& [l, f] : deco.faces)
            assert(f.segments.size() == 3);

        deco.populate();

        for (auto& [i, s] : deco.spheres) {
            fmt::print("{{id: {}, r: {}, R: {}, V: {}, A: {}, S: {} }}\n", s.id,
                       s.r, s.R, s.V, s.A, s.S);
        }
    }
}
