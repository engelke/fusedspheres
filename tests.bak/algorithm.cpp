#include "fusedspheres/algorithm.hpp"
#include <cassert>
#include <set>

int main()
{
    using namespace fusedspheres;

    std::set<int> a{1, 2, 3}, b{2, 3, 4};
    assert((set_intersection(a, b) == std::set<int>{2, 3}));
    assert((set_difference(a, b) == std::set<int>{1}));
    assert((set_difference(b, a) == std::set<int>{4}));
}
