#include "fusedspheres/algorithm.hpp"
#include <fmt/ranges.h>
#include <string>
#include <vector>

void test(int i1, int i2, std::vector<char> expected)
{
    using namespace std;
    using namespace fusedspheres;

    vector<char> v{'a', 'b', 'c', 'd', 'e', 'f'};
    auto v_orig = v;
    char e1 = 'g';
    char e2 = 'h';

    splice(v, i1, i2, e1, e2);

    bool success = equal(begin(expected), end(expected), begin(v));
    fmt::print("splice({}, {}, {}, {}, {}) = {}? {}\n", v_orig, i1, i2, e1, e2,
               expected,
               success ? "\033[1;32mOK\033[0m" : "\033[1;31mFAIL\033[0m");


    if (!success)
    {
        fmt::print("{}\n", v);
        exit(EXIT_FAILURE);
    }
}

int main()
{
    test(0, 0, {'g', 'h', 'a', 'b', 'c', 'd', 'e', 'f'});
    test(1, 1, {'a', 'g', 'h', 'b', 'c', 'd', 'e', 'f'});
    test(6, 6, {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'});

    test(0, 1, {'g', 'h', 'b', 'c', 'd', 'e', 'f'});
    test(1, 2, {'a', 'g', 'h', 'c', 'd', 'e', 'f'});
    test(5, 6, {'a', 'b', 'c', 'd', 'e', 'g', 'h'});

    test(0, 4, {'g', 'h', 'e', 'f'});
    test(1, 5, {'a', 'g', 'h', 'f'});
    test(2, 6, {'a', 'b', 'g', 'h'});

    test(1, 0, {'a', 'g', 'h'});
    test(2, 1, {'b', 'g', 'h'});
    test(6, 5, {'f', 'g', 'h'});
}
