#include "fusedspheres/decomposition.hpp"
using namespace fusedspheres;

#include <fmt/ostream.h>
#include <fmt/ranges.h>

const std::string OK = "\033[1;32mOK\033[0m";
const std::string FAIL = "\033[1;31mFAIL\033[0m";

int main()
{
    fmt::print("Insertion tests\n");
    {
        fmt::print("Test 1: ");
        Decomposition deco;
        deco.insert_sphere(0, {0, 0, 0}, 1);
        deco.insert_sphere(1, {0, 0, 1.99}, 1);
        deco.insert_sphere(2, {0.8, 0, 1}, 1);

        deco.populate();

        if (deco.N1 != 3 || deco.N2 != 2)
        {
            fmt::print("{}\n", FAIL);
            fmt::print(" > N1 = {}, N2 = {}\n", deco.N1, deco.N2);
        }
        else
        {
            fmt::print("{}\n", OK);
        }
    }
    {
        fmt::print("Test 2: ");
        Decomposition deco;
        deco.insert_sphere(0, {0, 0, 0}, 2);
        deco.insert_sphere(1, {0, 0, 2}, 2);
        deco.insert_sphere(2, {0, 0, 1}, 1);

        deco.populate();

        if (deco.N1 != 2 || deco.N2 != 1)
        {
            fmt::print("{}\n", FAIL);
            fmt::print(" > N1 = {}, N2 = {}\n", deco.N1, deco.N2);
        }
        else
        {
            fmt::print("{}\n", OK);
        }
    }
    {
        fmt::print("Test 3: ");
        Decomposition deco;
        deco.insert_sphere(0, {0, 0, 0}, 2);
        deco.insert_sphere(1, {0, 0, 1}, 1);
        deco.insert_sphere(2, {0, 0, 2}, 2);

        deco.populate();

        if (deco.N1 != 2 || deco.N2 != 1)
        {
            fmt::print("{}\n", FAIL);
            fmt::print(" > N1 = {}, N2 = {}\n", deco.N1, deco.N2);
            // return EXIT_FAILURE;
        }
        else
        {
            fmt::print("{}\n", OK);
        }
    }
    {
        fmt::print("Test 4: ");
        Decomposition deco;
        deco.insert_sphere(0, {0, 0, 1}, 1);
        deco.insert_sphere(1, {0, 0, 2}, 2);
        deco.insert_sphere(2, {0, 0, 0}, 2);

        deco.populate();

        if (deco.N1 != 2 || deco.N2 != 1)
        {
            fmt::print("{}\n", FAIL);
            fmt::print(" > N1 = {}, N2 = {}\n", deco.N1, deco.N2);
        }
        else
        {
            fmt::print("{}\n", OK);
        }
    }
    {
        fmt::print("Test 5: ");
        Decomposition deco;
        deco.insert_sphere(0, {0, 0, 0}, 1);
        deco.insert_sphere(1, {1, 0, 0}, 1);
        deco.insert_sphere(2, {1, 1, 0}, 1);
        deco.insert_sphere(3, {0, 0, 1}, 1);

        deco.populate();

        if (deco.N1 != 4 || deco.N2 != 6)
        {
            fmt::print("{}\n", FAIL);
            fmt::print(" > N1 = {}, N2 = {}\n", deco.N1, deco.N2);
        }
        else
        {
            fmt::print("{}\n", OK);
        }
    }
    {
        fmt::print("Test 6: ");
        Decomposition deco;
        deco.insert_sphere(0, {0, 0, 0}, 1);
        deco.insert_sphere(1, {1, 0, 0}, 1);
        deco.insert_sphere(2, {0, 1, 0}, 1);
        deco.insert_sphere(3, {1, 1, 0}, 1);
        deco.insert_sphere(4, {0, 0, 1}, 1);
        deco.insert_sphere(5, {1, 0, 1}, 1);
        deco.insert_sphere(6, {0, 1, 1}, 1);
        deco.insert_sphere(7, {1, 1, 1}, 1);

        deco.populate();

        if (deco.N1 != 8 || deco.N2 != 12)
        {
            fmt::print("{}\n", FAIL);
            fmt::print(" > N1 = {}, N2 = {}\n", deco.N1, deco.N2);
        }
        else
        {
            fmt::print("{}\n", OK);
        }
    }
}
