#include "fusedspheres/types.hpp"

#include <fmt/core.h>
#include <fmt/ostream.h>
#include <fmt/ranges.h>

#define FS_NAME(arg, ...) #arg

#define FS_TEST(testfun, ...)                                                  \
    {                                                                          \
        fmt::print(" * {} on {}: ", #testfun, FS_NAME(__VA_ARGS__));           \
        bool success = testfun(__VA_ARGS__);                                   \
        if (success)                                                           \
        {                                                                      \
            fmt::print("\033[1;32mOK\033[0m\n");                               \
        }                                                                      \
        else                                                                   \
        {                                                                      \
            fmt::print("\033[1;31mFAIL\033[0m\n");                             \
            print_values(1, __VA_ARGS__);                                      \
            exit(EXIT_FAILURE);                                                \
        }                                                                      \
    }


template <class T>
void print_value(int n, const T& v)
{
    fmt::print("Operand {}:\n{}\n\n", n, v);
}
template <>
void print_value(int n, const Eigen::SparseMatrix<double>& v)
{
    fmt::print("Operand {}:\n{}\n\n", n, Eigen::MatrixXd(v));
}

template <class T, class... Ts>
void print_values(int n, const T& v, const Ts&... vs)
{
    print_value(n, v);
    if constexpr (sizeof...(Ts) > 0) print_values(n + 1, vs...);
}

const double eps = 1e-4;


template <class T1, class T2>
inline bool is_equal(T1 a, T2 b)
{
    return a == b;
}

template <>
inline bool is_equal(double a, double b)
{
    return fabs(2. * (a - b) / (a + b)) < eps;
}

template <>
inline bool is_equal(Eigen::MatrixXd a, Eigen::MatrixXd b)
{
    return 2 * (a - b).norm() / (a.norm() + b.norm()) < eps;
}
template <>
inline bool is_equal(Eigen::SparseMatrix<double> c, Eigen::MatrixXd b)
{
    Eigen::MatrixXd a = c;
    return 2 * (a - b).norm() / (a.norm() + b.norm()) < eps;
}

template <class T1, class T2>
inline bool is_greater(T1 a, T2 b)
{
    return a > b;
}

template <class T1, class T2>
inline bool is_greater_or_equal(T1 a, T2 b)
{
    return a >= b;
}

template <class T1, class T2>
inline bool is_less(T1 a, T2 b)
{
    return a < b;
}

template <class T1, class T2>
inline bool is_less_or_equal(T1 a, T2 b)
{
    return a <= b;
}


inline bool is_zero(Eigen::MatrixXd m) { return m.norm() < eps; }

inline bool is_symmetric(Eigen::MatrixXd m)
{
    return (m - m.transpose()).norm() / m.norm() < eps;
}


// inline void test(std::string msg, bool success)
// {
//     fmt::print("{}: {}\n", msg,
//                success ? "\033[1;32mOK\033[0m" : "\033[1;31mFAIL\033[0m");
//
//     if (!success) exit(EXIT_FAILURE);
// }
