#include "fusedspheres/decomposition.hpp"

int main() {
    using namespace fusedspheres;

    Face f(0, 1);
    f.R = 1;
    f.r = {0, 0, 0};
    f.n = {0, 0, 1};
    f.P << 1, 0, 0, 0, 1, 0;
    f.segments.push_back({{f.R, 0}, 2 * M_PI});

    /* check parallel faces */
    auto t = f.truncate({0, 0, 1}, {0, 0, 1});
    using Trunc = Face::TruncationType;
    assert(t == Trunc::None);

    t = f.truncate({0, 0, 1}, {0, 0, -1});
    assert(t == Trunc::Full);

    t = f.truncate({0, 0, -1}, {0, 0, 1});
    assert(t == Trunc::Full);

    t = f.truncate({0, 0, -1}, {0, 0, -1});
    assert(t == Trunc::None);

    /* check intersections outside disk */
    t = f.truncate({0, 0, 2}, {0, 1. / sqrt(2), 1. / sqrt(2)});
    assert(t == Trunc::None);

    t = f.truncate({0, 0, 2}, {0, -1. / sqrt(2), -1. / sqrt(2)});
    assert(t == Trunc::Full);

    /* check partial truncation */
    t = f.truncate({0.5, 0, 0}, {1. / sqrt(2), 0., 1. / sqrt(2)});
    assert(t == Trunc::Partial);
}
