#include "fusedspheres/core/decomposition.hpp"
#include "fusedspheres/extern/cxxopts.hpp"
#include "fusedspheres/io/csv.hpp"
#include "fusedspheres/mesh/cmap.hpp"
#include "fusedspheres/mesh/mesh.hpp"
#include "fusedspheres/mesh/wavefront.hpp"

#include <fstream>
#include <iostream>

int main(int argc, char** argv) {
    using namespace fusedspheres;
    using namespace cxxopts;

    /* define parameters */
    std::string input;
    std::string prefix;
    int subdivisions;
    std::string cmap_name;
    double cmin, cmax;
    bool colour_id;

    /* read commandline arguments */
    Options opts(argv[0], "Meshed Truncated Spheres");
    // clang-format off
    opts.add_options()
        ("h,help", "Show this help message")
        ("input", "Input filename (\"-\" for stdin)", value(input))
        ("output", "Output filename prefix", value(prefix))
        ("n,subdivisions", "Subdivisions of icosphere", value(subdivisions)->default_value("2"))
        ("l,min", "Lower boundary for colour map", value(cmin)->default_value("0.0"))
        ("u,max", "Upper boundary for colour map", value(cmax)->default_value("1.0"))
        ("c,cmap", "Name of colour map", value(cmap_name)->default_value("jet"))
        ("i,id", "Use index for colouring", value(colour_id))
    ;
    // clang-format on
    try {
        opts.parse_positional({"input", "output"});
        opts.positional_help({"INPUT OUTPUT"});
        auto result = opts.parse(argc, argv);

        if (result.count("help")) {
            fmt::print(stderr, "{}\n", opts.help({""}));
            exit(EXIT_SUCCESS);
        }
        if (!result.count("input"))
            throw_or_mimic<option_required_exception>("input");
        if (!result.count("output"))
            throw_or_mimic<option_required_exception>("output");
    } catch (const OptionException& e) {
        fmt::print(
            stderr,
            "Error during parsing of commandline options! See {} --help \n{}\n",
            argv[0], e.what());
        exit(EXIT_FAILURE);
    }

    /* read spheres from file */
    std::vector<std::tuple<Vec3d, double, double>> spheres;
    if (input == "-") {
        fmt::print(stderr, "Reading from stdin\n");
        io::read_txt(std::cin, std::back_inserter(spheres));
    } else {
        fmt::print(stderr, "Reading from {}\n", input);
        auto in = std::ifstream(input);
        io::read_txt(in, std::back_inserter(spheres));
    }
    fmt::print(stderr, "Decomposing {} spheres\n", spheres.size());

    /* decompose system */
    Decomposition deco;
    for (size_t i = 0; i < spheres.size(); ++i) {
        auto& [r, R, c] = spheres[i];
        deco.insert_sphere(i, r, R);

        if (colour_id)
            c = i * 1. / (spheres.size() - 1);
        else
            c = (c - cmin) / (cmin - cmax);
    }

    fmt::print(stderr, "Opening object file {0}.obj\n", prefix);
    auto obj_file = std::fopen(fmt::format("{}.obj", prefix).c_str(), "w");
    fmt::print(stderr, "Opening material file {0}.obj\n", prefix);
    auto mtl_file = std::fopen(fmt::format("{}.mtl", prefix).c_str(), "w");

    fmt::print(stderr, "Starting meshing\n");
    int vertex_cnt = 1;
    int mesh_cnt = 0;
    for (auto& [i, s] : deco.spheres) {
        auto m = mesh::create_icosphere(s.r, s.R, subdivisions);
        for (auto l : s.faces) {
            auto& f = deco.faces[l];
            if (f.s[0] == s.id)
                mesh::truncate(m, f.n, f.v[0]);
            else
                mesh::truncate(m, -f.n, f.v[1]);
        }

        auto c = cmap::jet(std::get<2>(spheres[i]));
        auto mat_name = fmt::format("m{}", s.id);
        wavefront::write_mtl(mtl_file, mat_name, c);
        fmt::print(obj_file, "usemtl {}\n", mat_name);
        wavefront::write_obj(obj_file, fmt::format("p{}", s.id), m, vertex_cnt);

        vertex_cnt += m.vertices.size();
        ++mesh_cnt;
    }

    std::fclose(obj_file);
    std::fclose(mtl_file);
    fmt::print(stderr, "Done\n");
}
