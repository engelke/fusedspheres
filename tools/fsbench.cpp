#include "fusedspheres/core/decomposition.hpp"
#include "fusedspheres/extern/cxxopts.hpp"
#include "fusedspheres/io/csv.hpp"

#include <chrono>
#include <ctime>
#include <fstream>
#include <iostream>
#include <map>
using namespace fusedspheres;

using TupleSphere = std::tuple<Vec3d, double>;

class Timer {
    using clock = std::chrono::high_resolution_clock;
    using TimePoint = std::chrono::time_point<clock>;

    std::clock_t m_cpu_start;
    TimePoint m_wall_start;

    double m_cpu_passed = 0;
    double m_wall_passed = 0;

public:
    inline void start() {
        m_cpu_start = std::clock();
        m_wall_start = clock::now();
    }
    inline void stop() {
        m_cpu_passed += 1e6 * (std::clock() - m_cpu_start) / CLOCKS_PER_SEC;
        m_wall_passed += std::chrono::duration_cast<std::chrono::microseconds>(
                             clock::now() - m_wall_start)
                             .count();
    }
    inline void reset() {
        m_cpu_passed = 0;
        m_wall_passed = 0;
    }
    inline double get() { return m_cpu_passed; }
    inline double get_wall() { return m_wall_passed; }
};

template <class It> auto benchmark(It first, It last, int n) {
    std::map<std::string, Timer> timers;
    Jacobians J;
    timers["total"].start();
    for (int i = 0; i < n; ++i) {
        timers["insert"].start();
        Decomposition deco;

        /* set octree insertion */
        Vec3d xmin = Vec3d::Constant(1e300);
        Vec3d xmax = Vec3d::Constant(-1e300);
        for (auto it = first; it != last; ++it) {
            const auto& [x, R] = *it;
            for (int d = 0; d < 3; ++d) {
                xmin[d] = std::min(x[d] - R, xmin[d]);
                xmax[d] = std::max(x[d] + R, xmax[d]);
            }
        }
        xmin = xmin.array() + 0.1;
        double l = (xmax - xmin).maxCoeff() + 0.1;
        deco.set_octree(xmin, l);

        int id = 0;
        for (auto it = first; it != last; ++it) {
            auto& [x, R] = *it;
            deco.insert_sphere(id++, x, R);
        }
        deco.enumerate();
        timers["insert"].stop();

        timers["populate"].start();
        deco.calculate_NVAS();
        timers["populate"].stop();

        timers["jacobians"].start();
        deco.derive(J);
        timers["jacobians"].stop();
    }
    timers["total"].stop();

    return timers;
}

void sort_block(std::vector<TupleSphere>& spheres) {
    Vec3d x0{0, 0, 0};
    for (auto& [x, R] : spheres)
        x0 += x;
    x0 *= 1. / spheres.size();

    auto dist = [&x0](const TupleSphere& s) -> double {
        return (std::get<0>(s) - x0).cwiseAbs().sum();
    };

    std::sort(spheres.begin(), spheres.end(),
              [dist](auto& a, auto& b) { return dist(a) < dist(b); });
}

template <class Arg, class... Args>
void print_row(fmt::string_view form, Arg&& arg, Args&&... args) {
    fmt::vprint(form, fmt::make_format_args(std::forward<Arg>(arg)));
    if constexpr (sizeof...(args) > 0)
        print_row(form, std::forward<Args>(args)...);
    else
        fmt::print("\n");

    fflush(stdout);
}

int main(int argc, char** argv) {
    using namespace fusedspheres;
    using namespace cxxopts;

    std::string input;
    double s;
    int nrep;
    int niter;

    Options opts(argv[0], "Meshed Truncated Spheres");
    // clang-format off
    opts.add_options()
        ("h,help", "Show this help message")
        ("n,nrep", "Number of repetitions", value(nrep)->default_value("100"))
        ("s,scale", "Scale radius", value(s)->default_value("1"))
        ("i,iter", "Iterate particle numbers", value(niter)->default_value("1"))
        ("input", "Input filename (\"-\" for stdin)", value(input))
    ;
    // clang-format on
    try {
        opts.parse_positional({"input"});
        opts.positional_help({"INPUT"});
        auto result = opts.parse(argc, argv);

        if (result.count("help")) {
            fmt::print(stderr, "{}\n", opts.help({""}));
            exit(EXIT_SUCCESS);
        }
    } catch (const OptionException& e) {
        fmt::print(
            stderr,
            "Error during parsing of commandline options! See {} --help \n{}\n",
            argv[0], e.what());
        exit(EXIT_FAILURE);
    }

    std::vector<std::tuple<Vec3d, double>> spheres;
    if (input == "-") {
        fmt::print(stderr, "Reading from stdin\n");
        io::read_txt(std::cin, std::back_inserter(spheres));
    } else {
        fmt::print(stderr, "Reading from {}\n", input);
        auto in = std::ifstream(input);
        io::read_txt(in, std::back_inserter(spheres));
    }

    for (auto& [x, R] : spheres)
        R *= s;

    // fmt::print("{}\nProperties=pos:R:3:radius:R:1\n", 100);
    // for (size_t i = 0; i < 100; ++i)
    // {
    //     auto& [x, R] = spheres[i];
    //     fmt::print("{} {} {} {}\n", x[0], x[1], x[2], R);
    // }

    print_row("{:<12}", "number", "total", "insert", "populate", "jacobians");

    if (niter > 1) {
        sort_block(spheres);
        double ninc = std::pow(spheres.size(), 1. / niter);
        int n = ninc;
        for (int i = 0; i < niter; ++i) {
            auto t = benchmark(spheres.begin(), spheres.begin() + n, nrep);

            print_row("{:<12}", n, t["total"].get_wall() / nrep,
                      t["insert"].get_wall() / nrep,
                      t["populate"].get_wall() / nrep,
                      t["jacobians"].get_wall() / nrep);
            n = std::min<int>(spheres.size(), n * ninc);
        }
    } else {
        auto t = benchmark(spheres.begin(), spheres.end(), nrep);
        print_row("{:<12}", spheres.size(), t["total"].get_wall(),
                  t["insert"].get_wall(), t["populate"].get_wall(),
                  t["jacobians"].get_wall());
    }
}
