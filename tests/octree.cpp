#include <algorithm>
#include <fmt/color.h>
#include <fmt/ranges.h>
#include <forward_list>

#include "fusedspheres/core/octree.hpp"
using fusedspheres::octree::Octree;

#include "fusedspheres/core/decomposition.hpp"
#include "fusedspheres/core/linalg.hpp"
#include "fusedspheres/random.hpp"
using namespace fusedspheres;

auto find_contacts_simple(const auto& spheres) {
    std::vector<std::array<int, 2>> contacts;

    int n = spheres.size();
    int cnt = 0;
    for (int i = 0; i < n; ++i) {
        for (int j = i + 1; j < n; ++j) {
            ++cnt;
            if ((spheres[i].r - spheres[j].r).norm() <
                spheres[i].R + spheres[j].R)
                contacts.push_back({i, j});
        }
    }

    // fmt::print("Octree: number of contacts checked {}\n", cnt);

    return contacts;
}

/**
 * @brief Return lower boundary and cube length
 */
auto bounding_cube(const auto& spheres) {
    Vec3d xl = Vec3d::Constant(1e300);
    Vec3d xh = Vec3d::Constant(-1e300);

    for (auto s : spheres) {
        for (int i = 0; i < 3; ++i) {
            xl[i] = std::min(xl[i], s.r[i] - s.R - eps);
            xh[i] = std::max(xh[i], s.r[i] + s.R + eps);
        }
    }
    xh -= xl;

    return std::pair{xl, xh.maxCoeff()};
}

template <typename T> void print_octree(const T& oct) {
    using Node = typename T::Node;
    std::forward_list<std::pair<Node*, int>> stack;
    stack.push_front({oct.root().get(), 0});
    fmt::print("Octree: \n");

    while (!stack.empty()) {
        auto [node, level] = stack.front();
        stack.pop_front();

        auto indent = std::string(2 * level, ' ');
        fmt::print("{}", indent);
        if (node->has_value())
            fmt::print("{}\n", node->value());
        else
            fmt::print("empty\n");

        for (auto c : node->children()) {
            stack.push_front({c, level + 1});
        }
    }
}

auto find_contacts_octree(const auto& spheres) {
    /* create octree */
    auto [x0, l] = bounding_cube(spheres);
    auto oct = Octree<std::vector<int>>(x0, l);

    int n = spheres.size();
    for (int i = 0; i < n; ++i) {
        oct.get(spheres[i].r, spheres[i].R).push_back(i);
    }

    // print_octree(oct);

    std::vector<std::array<int, 2>> contacts;
    int cnt = 0;
    for (int i = 0; i < n; ++i) {
        auto& s = spheres[i];
        for (auto js : oct.traverse(s.r, s.R)) {
            for (auto j : js) {
                auto& o = spheres[j];
                cnt += i < j;
                if (i < j && (s.r - o.r).norm() < s.R + o.R)
                    contacts.push_back({i, j});
            }
        }
    }

    // fmt::print("Octree: number of contacts checked {}\n", cnt);

    /* only necessary for comparison with simple method */
    std::sort(contacts.begin(), contacts.end());
    return contacts;
}

bool test_octree_contacts() {
    fmt::print("Octree contacts: ");

    /* create test data */
    std::vector<Sphere> spheres;
    int N = 1000;
    RandomReal<double> rng(1);
    double l = 15;
    for (int i = 0; i < N; ++i) {
        spheres.emplace_back(i, Vec3d{l * rng(), l * rng(), l * rng()},
                             0.05 + 0.95 * rng());
    }

    auto r1 = find_contacts_simple(spheres);
    auto r2 = find_contacts_octree(spheres);

    auto success = std::equal(r1.begin(), r1.end(), r2.begin(), r2.end());

    if (success)
        fmt::print(fmt::emphasis::bold | fmt::fg(fmt::color::green), "OK\n");
    else
        fmt::print(fmt::emphasis::bold | fmt::fg(fmt::color::red), "FAIL\n");

    return success;
}

bool test_octree_decomposition() {
    fmt::print("Octree decomposition: ");

    std::vector<Sphere> spheres;
    int N = 40;
    RandomReal<double> rng(1);
    double l = 5;
    for (int i = 0; i < N; ++i) {
        spheres.emplace_back(i, Vec3d{l * rng(), l * rng(), l * rng()},
                             0.05 + 0.95 * rng());
    }

    // spheres.emplace_back(0, Vec3d{0, 0, 0}, 1);
    // spheres.emplace_back(1, Vec3d{1, 0, 0}, 1);

    double dV, dA, dS;

    /* using simple n^2 */
    {
        auto deco = Decomposition();

        for (const auto& s : spheres)
            deco.insert_sphere(s);

        deco.enumerate();
        auto [N, V, A, S] = deco.calculate_NVAS();
        dV = V.sum();
        dA = A.sum();
        dS = S.sum();
    }

    /* using octree */
    {
        auto deco = Decomposition();
        auto [x0, l] = bounding_cube(spheres);
        deco.set_octree(x0, l);

        for (const auto& s : spheres) {
            deco.insert_sphere(s);
            // fmt::print("After inserting {}\n", s.id);
            // print_octree(deco.octree());
        }

        deco.enumerate();
        auto [N, V, A, S] = deco.calculate_NVAS();
        dV -= V.sum();
        dA -= A.sum();
        dS -= S.sum();
    }

    bool success = fabs(dV) < eps && fabs(dA) < eps && fabs(dS) < eps;
    if (success)
        fmt::print(fmt::emphasis::bold | fmt::fg(fmt::color::green), "OK\n");
    else
        fmt::print(fmt::emphasis::bold | fmt::fg(fmt::color::red), "FAIL\n");
    return success;
}

int main() {
    bool success = true;

    success &= test_octree_contacts();
    success &= test_octree_decomposition();
    return success ? EXIT_SUCCESS : EXIT_FAILURE;
}
