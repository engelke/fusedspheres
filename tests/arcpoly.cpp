#include "fusedspheres/core/arcpoly.hpp"
#include <fmt/color.h>
#include <fmt/core.h>

using namespace fusedspheres;

void print_success(bool success) {
    if (success)
        fmt::print(fmt::emphasis::bold | fmt::fg(fmt::color::green), "OK\n");
    else
        fmt::print(fmt::emphasis::bold | fmt::fg(fmt::color::red), "FAIL\n");
}

int main() {
    int cnt = 0;
    {
        fmt::print("Test {}: Quarter\n", ++cnt);
        ArcPoly ap{1};

        ap.truncate({1, 0}, 0);
        ap.truncate({0, 1}, 0);
        ap.populate();
        double dA = ap.area - M_PI / 4;
        fmt::print("A = {:<9.7f}; dA = {:<9.7f}; ", ap.area, dA);
        print_success(fabs(dA) < 1e-8);
    }
    {
        fmt::print("Test {}: Half circle 1\n", ++cnt);
        ArcPoly ap{1};

        ap.truncate({1, 0}, 0);
        ap.truncate({1, 0}, 0.5);
        ap.populate();
        double dA = ap.area - M_PI / 2;
        fmt::print("A = {:<9.7f}; dA = {:<9.7f}; ", ap.area, dA);
        print_success(fabs(dA) < 1e-8);
    }
    {
        fmt::print("Test {}: Half circle 2\n", ++cnt);
        ArcPoly ap{1};

        ap.truncate({1, 0}, 0.5);
        ap.truncate({1, 0}, 0);
        ap.populate();
        double dA = ap.area - M_PI / 2;
        fmt::print("A = {:<9.7f}; dA = {:<9.7f}; ", ap.area, dA);
        print_success(fabs(dA) < 1e-8);
    }
    {
        fmt::print("Test {}: Strip\n", ++cnt);
        ArcPoly ap{1};

        ap.truncate({1, 0}, 0.25);
        ap.truncate({-1, 0}, 0.25);
        ap.populate();
        double dA = ap.area - 0.98948343;
        fmt::print("A = {:<9.7f}; dA = {:<9.7f}; ", ap.area, dA);
        print_success(fabs(dA) < 1e-8);
    }
    {
        fmt::print("Test {}: Empty ", ++cnt);
        ArcPoly ap{1};

        ap.truncate({1, 0}, -0.25);
        print_success(!ap.truncate({-1, 0}, -0.25));
    }
    {
        fmt::print("Test {}: Strip\n", ++cnt);
        ArcPoly ap{1};

        ap.truncate({1, 0}, 0.25);
        ap.truncate({-1, 0}, 0.25);
        ap.populate();
        double dA = ap.area - 0.98948343;
        fmt::print("A = {:<9.7f}; dA = {:<9.7f}; ", ap.area, dA);
        print_success(fabs(dA) < 1e-8);
    }
    {
        fmt::print("Test {}: Square\n", ++cnt);
        ArcPoly ap{1};

        ap.truncate({1, 0}, 0.25);
        ap.truncate({-1, 0}, 0.25);
        ap.truncate({0, 1}, 0.25);
        ap.truncate({0, -1}, 0.25);
        ap.populate();
        double dA = ap.area - 0.25;
        fmt::print("A = {:<9.7f}; dA = {:<9.7f}; ", ap.area, dA);
        print_success(fabs(dA) < 1e-8);
    }
}