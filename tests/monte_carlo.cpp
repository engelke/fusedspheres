#include "fusedspheres/core/decomposition.hpp"
#include "fusedspheres/random.hpp"

#include <fmt/core.h>
#include <fmt/ostream.h>
#include <fmt/ranges.h>

using namespace fusedspheres;

const Face& getf(const Decomposition& deco, int l) { return deco.faces.at(l); }
const Sphere& gets(const Decomposition& deco, int i) {
    return deco.spheres.at(i);
}

auto pass_string(bool pass) {
    return pass ? "\033[1;32mOK\033[0m" : "\033[1;31mFAIL\033[0m";
}

double sample_volume(const std::vector<Sphere>& spheres, int n) {
    Arr3d x0{1e10, 1e10, 1e10};
    Arr3d l = -x0;
    for (auto& s : spheres) {
        for (int d = 0; d < 3; ++d) {
            x0[d] = std::min(x0[d], s.r[d] - s.R - 0.1);
            l[d] = std::max(l[d], s.r[d] + s.R + 0.1);
        }
    }
    l -= x0;

    auto rng = Sobol<Arr3d>();

    int i = 0;
    for (int iter = 0; iter < n; ++iter) {
        Vec3d x = x0 + l * rng();
        for (auto& s : spheres) {
            if ((s.r - x).squaredNorm() < s.R * s.R) {
                ++i;
                break;
            }
        }
    }

    return i * l[0] * l[1] * l[2] / n;
}

double sample_sphere(Decomposition& deco, int id, int n, unsigned seed) {
    auto& s = deco.spheres.at(id);
    // RandomReal<double> rng(seed, -s.R, s.R);
    auto rng = Sobol<Arr3d>();

    int i = 0;
    for (int iter = 0; iter < n; ++iter) {
        Vec3d x = s.R * (2 * rng() - 1);
        if (x.squaredNorm() < s.R * s.R) {
            x += s.r;
            for (auto l : s.faces) {
                auto& f = deco.faces.at(l);
                auto pm = f.s[0] == s.id ? 1 : -1;
                if (pm * (x - f.r).dot(f.n) > 0)
                    goto next;
            }
            ++i;
        }
    next:;
    }

    return i * 8 * s.R * s.R * s.R / n;
}

bool check(const std::vector<Sphere>& spheres, int niter, double eps) {
    fmt::print("samples: {}\ntolerance: {}\n", niter, eps);
    Decomposition deco;
    for (auto& s : spheres)
        deco.insert_sphere(s);

    deco.enumerate();
    auto [N, V, A, S] = deco.calculate_NVAS();
    double Vd = V.sum();
    double Vm = sample_volume(spheres, niter);
    bool pass = fabs((Vd - Vm) / Vd) < eps;

    fmt::print("\n  {:3} {:9} {:9}\n", "id", "analytic", "MC");
    fmt::print("> tot {:<9.7} {:<9.7} {}\n", Vd, Vm, pass_string(pass));
    for (auto& [i, s] : deco.spheres) {
        auto Vmi = sample_sphere(deco, i, niter, 0);
        Vm -= Vmi;
        auto pass_sphere = fabs((V[s.row] - Vmi) / V[s.row]) < eps;

        fmt::print("> {:<3} {:<9.7} {:<9.7} {}\n", i, V[s.row], Vmi,
                   pass_string(pass_sphere));

        pass &= pass_sphere;
    }

    fmt::print("\nMC: V - sum(V_i) = {}\n", Vm);

    fmt::print("\nPass: {}\n\n", pass_string(pass));
    return pass;
}

int main() {
    int cnt = 0;
    {
        fmt::print("Monte Carlo Test {}: Unequal pair\n", cnt++);
        std::vector<Sphere> spheres;
        int id = 0;
        spheres.emplace_back(id++, Vec3d{0, 0, 0}, 1);
        spheres.emplace_back(id++, Vec3d{0.5, 0, 0}, 0.6);

        check(spheres, 1e7, 2e-4);
    }
    {
        fmt::print("Monte Carlo Test {}: 3 spheres\n", cnt++);
        std::vector<Sphere> spheres;
        int id = 0;
        spheres.emplace_back(id++, Vec3d{0, 0, 0}, 1);
        spheres.emplace_back(id++, Vec3d{1, 0, 0}, 1);
        spheres.emplace_back(id++, Vec3d{.5, 1, 0}, 1);

        check(spheres, 1e7, 2e-4);
    }
    {
        fmt::print("Monte Carlo Test {}: 3 unequal spheres\n", cnt++);
        std::vector<Sphere> spheres;
        int id = 0;
        spheres.emplace_back(id++, Vec3d{0, 0, 0}, 1);
        spheres.emplace_back(id++, Vec3d{0.5, 0.1, 0}, 0.65);
        spheres.emplace_back(id++, Vec3d{0.5, -0.1, 0}, 0.7);

        check(spheres, 1e6, 1e-3);
    }
    // Still fails due to separated sub graph at decomposition.cpp:255
    // {
    //     fmt::print("Monte Carlo Test {}: 4 unequal spheres\n", cnt++);
    //     std::vector<Sphere> spheres;
    //     int id = 0;
    //     spheres.emplace_back(id++, Vec3d{0, 0, 0}, 1);
    //     spheres.emplace_back(id++, Vec3d{0.5, 0.1, 0}, 0.65);
    //     spheres.emplace_back(id++, Vec3d{0.5, -0.1, 0}, 0.7);
    //     spheres.emplace_back(id++, Vec3d{1.3, -0.1, 0.1}, 1.3);

    //     assert(check(spheres, 1e6, 1e-3));
    // }
    {
        fmt::print("Monte Carlo Test {}: Sphere in the middle 1\n", cnt++);
        std::vector<Sphere> spheres;
        int id = 0;
        spheres.emplace_back(id++, Vec3d{0, 0, 0}, 1);
        spheres.emplace_back(id++, Vec3d{0.5, 0, 0}, 0.6);
        spheres.emplace_back(id++, Vec3d{1, 0, 0}, 1);

        check(spheres, 1e7, 2e-4);
    }
    {
        fmt::print("Monte Carlo Test {}: Sphere in the middle 2\n", cnt++);
        std::vector<Sphere> spheres;
        int id = 0;
        spheres.emplace_back(id++, Vec3d{0.5, 0, 0}, 0.6);
        spheres.emplace_back(id++, Vec3d{0, 0, 0}, 1);
        spheres.emplace_back(id++, Vec3d{1, 0, 0}, 1);

        check(spheres, 1e7, 2e-4);
    }
    {
        fmt::print("Monte Carlo Test {}: Cube of spheres\n", cnt++);
        std::vector<Sphere> spheres;
        int id = 0;
        spheres.emplace_back(id++, Vec3d{0, 0, 0}, 1);
        spheres.emplace_back(id++, Vec3d{1, 0, 0}, 1);
        spheres.emplace_back(id++, Vec3d{0, 1, 0}, 1);
        spheres.emplace_back(id++, Vec3d{1, 1, 0}, 1);
        spheres.emplace_back(id++, Vec3d{0, 0, 1}, 1);
        spheres.emplace_back(id++, Vec3d{1, 0, 1}, 1);
        spheres.emplace_back(id++, Vec3d{0, 1, 1}, 1);
        spheres.emplace_back(id++, Vec3d{1, 1, 1}, 1);

        check(spheres, 1e7, 1e-4);
    }
    {
        fmt::print("Monte Carlo Test {}: Included spheres\n", cnt++);
        std::vector<Sphere> spheres;
        int id = 0;
        spheres.emplace_back(id++, Vec3d{0, 0, 0}, 2.1);
        spheres.emplace_back(id++, Vec3d{1, 0, 0}, 1);
        spheres.emplace_back(id++, Vec3d{0, 1, 0}, 1);
        spheres.emplace_back(id++, Vec3d{1, 1, 0}, 1);
        spheres.emplace_back(id++, Vec3d{0, 0, 1}, 1);
        spheres.emplace_back(id++, Vec3d{1, 0, 1}, 1);
        spheres.emplace_back(id++, Vec3d{0, 1, 1}, 1);
        spheres.emplace_back(id++, Vec3d{1, 1, 1}, 1);

        check(spheres, 1e7, 2e-4);
    }

    {
        fmt::print("Monte Carlo Test {}: Random\n", cnt++);
        std::vector<Sphere> spheres;
        int N = 40;
        RandomReal<double> rng(1);
        double l = 5;
        for (int i = 0; i < N; ++i) {
            spheres.emplace_back(i, Vec3d{l * rng(), l * rng(), l * rng()}, 1);
        }

        check(spheres, 1e7, 2e-3);
    }

    // {
    //     fmt::print("Monte Carlo Test {}: Failing random subset\n", cnt++);
    //     std::vector<Sphere> spheres;
    //     int id = 0;
    //     spheres.emplace_back(id++, Vec3d{0.247, 2.594, 0.861}, 1);
    //     spheres.emplace_back(id++, Vec3d{0.988, 1.448, 0.711}, 1);
    //     spheres.emplace_back(id++, Vec3d{0.597, 2.624, 0.418}, 1);
    //     // spheres.emplace_back(id++, Vec3d{0.700, 3.962, 0.149}, 1);
    //     // spheres.emplace_back(id++, Vec3d{0.058, 2.491, 0.369}, 1);
    //     spheres.emplace_back(id++, Vec3d{0.765, 2.893, 0.045}, 1);
    //     spheres.emplace_back(id++, Vec3d{1.152, 2.541, 1.043}, 1);

    //     assert(check(spheres, 1e6, 1e-3));
    // }
}
