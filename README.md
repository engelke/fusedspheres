<table align="center"><tr><td align="center" width="9999">
<img src="exploded.png" width=400px>

# Fused Spheres

C++ Truncated Sphere Algorithm
</td></tr></table>

This is a C++ library for decomposing overlapping spheres into disjoint regions, so called truncated spheres. Volume $`V_i`$, spherical surface area $`A_i`$ and area of individual faces $`S_{ij}`$ can be calculated. Furthermore, this library is able to compute the Jacobians of all quantities with respect to sphere positions $`\vec{x}_i`$ and radii $`R_i`$. Computations are thread-parallelised with OpenMP.

In addition to this, a tool for creating 3D objects of a decomposed system of spheres is provided, which produces Wavefront (.obj) files, that can be read by many 3D modeling softwares.

# Installation

### Requirements
* Compiler: 
    * g++ v10.2 or higher
    * clang v14.0 or higher (older version might work, but not tested)
* Libraries:
    * Eigen v3.4
    * libfmt v9.0 (only for tools and tests)

### Manual installation of tools and library
```sh
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make

# for installation without privileges choose ~/.local
# for system-wide installation e.g. /usr/local
cmake --install . --prefix /your/install/dir
```

### Use as a git submodule
When using cmake for building a project, the easiest way is to include this library as a submodule
```sh
# execute in your project's git repo
git submodule add git@gitlab.com:engelke/fusedspheres.git extern/fusedspheres
```
more on git submodules https://www.git-scm.com/book/en/v2/Git-Tools-Submodules

```cmake
# CMakeLists.txt
add_subdirectory(extern/fusedspheres)
include_directories(extern/fusedspheres/include)
link_directories(extern/fusedspheres)
```

# Usage

### Minimal example
```c++
/* the main decomposition class */
Decomposition deco;

/* insert sphere and decompose */
deco.insert_sphere(0, Vec3d{0, 0, 0}, 1); // id, centre, radius
deco.insert_sphere(1, Vec3d{1, 0, 0}, 1);
// and so on

/* create a continuous enumeration */
deco.enumerate();

/* calculate volumes etc. */
auto [N, V, A, S] = deco.calculate_NVAS(); 

/* calculate Jacobian matrices */
Jacobians J;
deco.derive(J);
```

### Explanation

The central class is `Decomposition`. At any time, it contains a valid decomposition of all inserted spheres. Each inserted sphere must have a unique id. Inserting multiple spheres with the same id will result in undefined behaviour. Inserted spheres are stored in the member variable `.spheres` and all faces between spheres are stored in a member variable `.faces`.

Before calculating volumes and areas, `.enumerate()` must be called. This method will enumerate all spheres and faces in the decomposition. The enumeration is stored in a member `.row` in every sphere and face.

After enumeration, the volumes etc. are calculated with `.calculate_NVAS()`. This methods returns a tuple of 4 values. 
| Name | Type                    | Size   | Description                                      |
|------|-------------------------|--------|--------------------------------------------------|
| N    | `std::array<size_t, 4>` | -      | Number of spheres, faces, lines and points       |
| V    | `Eigen::VectorXd`       | `N[0]` | Volumes of the truncated spheres                 |
| A    | `Eigen::VectorXd`       | `N[0]` | Spherical surface areas of the truncated spheres |
| S    | `Eigen::VectorXd`       | `N[1]` | Areas of the faces                               |

The derivatives of each volume, surface or interface area with respect to the sphere centre and radius is calculated by `.derive(J)` using finite differences. This method is computationally expensive, because for each sphere a partial decomposition needs to be calculated $`8 (Z_i + 1)`$ times, where $`Z_i`$ is the number of neighbours of sphere $`i`$

