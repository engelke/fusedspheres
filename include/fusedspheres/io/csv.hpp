#ifndef FUSEDSPHERES_IO_CSV_HPP
#define FUSEDSPHERES_IO_CSV_HPP
// #include "fusedspheres/truncated_sphere.hpp"
#include "fusedspheres/io/traits.hpp"
#include <fmt/ranges.h>
#include <istream>
#include <sstream>
#include <tuple>

namespace fusedspheres::io
{
    namespace details
    {
        /* default deserialization for types that support operator>> */
        template <class T>
        auto deserialize(std::istream& in, T&& v = T()) -> decltype(in >> v, v)
        {
            in >> v;
            return v;
        }

        /* deserialization for iterable containers */
        template <class T>
        auto deserialize(std::istream& in, T&& vs = T()) -> std::enable_if_t<
            is_iterable<std::remove_reference_t<T>>::value,
            decltype(in >> std::declval<
                         typename std::remove_reference_t<T>::value_type&>(),
                     vs)>
        {
            for (auto& v : vs)
                deserialize(in, v);
            return vs;
        }

        template <size_t I = 0, class... Ts>
        void deserialize_tuple(std::istream& in, std::tuple<Ts...>& tuple)
        {
            deserialize(in, std::get<I>(tuple));
            if constexpr (I < sizeof...(Ts) - 1)
                deserialize_tuple<I + 1>(in, tuple);
        }

        /* deserialization for tuples */
        template <class T>
        auto deserialize(std::istream& in, T&& v = T())
            -> std::enable_if_t<is_tuple<std::remove_reference_t<T>>::value, T>
        {
            deserialize_tuple(in, v);
            return v;
        }
    } // namespace details

    template <class Out>
    Out read_txt(std::istream& in, Out out)
    {
        for (std::string line; std::getline(in, line);)
        {
            std::istringstream s(line);
            using Value = typename Out::container_type::value_type;
            *out++ = details::deserialize<Value>(s);
        }
        return out;
    }
} // namespace fusedspheres::io

#endif /* end of include guard: FUSEDSPHERES_IO_CSV_HPP */

#include <istream>
