#ifndef FUSEDSPHERES_IO_TRAITS_HPP
#define FUSEDSPHERES_IO_TRAITS_HPP
#include <iterator>
#include <type_traits>

namespace fusedspheres::io
{
    template <typename T, typename = void>
    struct is_iterable : std::false_type
    {
    };

    template <typename T>
    struct is_iterable<T,
                       std::void_t<decltype(std::begin(std::declval<T>())),
                                   decltype(std::end(std::declval<T>()))>>
        : std::true_type
    {
    };

    template <typename T>
    struct is_tuple : std::false_type
    {
    };

    template <typename... Ts>
    struct is_tuple<std::tuple<Ts...>> : std::true_type
    {
    };

} // namespace fusedspheres::io

#endif /* end of include guard: FUSEDSPHERES_IO_TRAITS_HPP */
