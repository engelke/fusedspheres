#pragma once
#include <Eigen/Dense>
#include <boost/random/sobol.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/random/variate_generator.hpp>
#include <random>

namespace fusedspheres {
template <typename RealType> class RandomReal {
public:
    using Distribution = std::uniform_real_distribution<RealType>;
    using Engine = std::mt19937;

    RandomReal(unsigned seed, RealType min = 0, RealType max = 1)
        : eng(seed), dis(min, max) {}
    RealType operator()() { return dis(eng); }

private:
    Engine eng;
    Distribution dis;
};

template <typename T> class Sobol {
public:
    using Dist = boost::random::uniform_01<typename T::Scalar>;
    using Engine = boost::random::sobol;
    using Gen = boost::random::variate_generator<Engine, Dist>;

    Sobol(unsigned seed = 0)
        : eng(T::SizeAtCompileTime), dist(), gen(eng, dist) {
        eng.seed(seed);
    }

    T operator()() {
        T v;
        std::generate(v.begin(), v.end(), [&]() { return gen(); });
        return v;
    }

private:
    Engine eng;
    Dist dist;
    Gen gen;
};

} // namespace fusedspheres
