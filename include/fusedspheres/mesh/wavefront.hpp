#ifndef FUSEDSPHERES_MESH_WAVEFRONT_HPP
#define FUSEDSPHERES_MESH_WAVEFRONT_HPP
#include "fusedspheres/mesh/cmap.hpp"
#include "fusedspheres/mesh/mesh.hpp"
#include <string>

namespace fusedspheres::wavefront
{
    void write_obj(FILE* file, std::string, const mesh::Mesh&, int n);
    void write_mtl(FILE* file, std::string, const cmap::Colour&);
} // namespace fusedspheres::wavefront

#endif /* end of include guard: FUSEDSPHERES_MESH_WAVEFRONT_HPP */
