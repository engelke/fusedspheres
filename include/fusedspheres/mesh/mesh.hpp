#pragma once
#include "fusedspheres/core/linalg.hpp"
#include "fusedspheres/core/map.hpp"
#include <array>
#include <set>
#include <unordered_map>
#include <vector>

namespace fusedspheres::mesh {
using Index = std::ptrdiff_t;
using Face = std::array<Index, 3>;
using Edge = std::array<Index, 2>;

struct Mesh {
    Vec3d origin;
    std::vector<Vec3d> vertices;
    std::vector<Face> faces;
    std::vector<int> groups; //< face group
    Set<int> smooth;
};

struct EdgeHash {
    std::size_t operator()(const Edge& e) const {
        std::size_t h1 = e[0];
        std::size_t h2 = e[1];
        if (h1 > h2)
            std::swap(h1, h2);
        return h1 ^ (h2 << 1);
    }
};
struct EdgeEqual {
    bool operator()(Edge e1, Edge e2) const {
        if (e1[0] > e1[1])
            std::swap(e1[0], e1[1]);
        if (e2[0] > e2[1])
            std::swap(e2[0], e2[1]);
        return e1 == e2;
    }
};

template <class T>
using EdgeMap = std::unordered_map<Edge, T, EdgeHash, EdgeEqual>;

Mesh create_icosphere(Vec3d, double, int);

void refine_subdivide_edges(Mesh&);
void remove_unconnected(Mesh&);

void transform_translate(Mesh&, Vec3d offset);
void transform_scale(Mesh&, double factor);

void truncate(Mesh&, Vec3d, double);

} // namespace fusedspheres::mesh
