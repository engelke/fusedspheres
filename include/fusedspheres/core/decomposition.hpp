#pragma once
#include "interface.hpp"
#include "map.hpp"
#include "octree.hpp"
#include "sphere.hpp"
#include <algorithm>
#include <fmt/core.h>

namespace fusedspheres {
using FaceMap = Map<int, Face>;
using SphereMap = Map<int, Sphere>;
using IdMap = Map<int, int>;
using IdSet = Set<int>;
using IdVec = std::vector<int>;

using octree::Octree;

enum class SphereIntersection { None, FirstIncluded, SecondIncluded, Regular };

struct SingleVAS {
    double V = 0;
    double A = 0;
    std::vector<std::pair<int, double>> S;
};

using Ranks = std::array<size_t, 4>;
using FullGeo = std::tuple<Ranks, VecXd, VecXd, VecXd>;

struct Triplets {
    std::array<std::vector<Eigen::Triplet<double>>, 4> V, A, S;
};

struct Jacobians {
    std::array<SpMat, 4> V, A, S;
};

class Decomposition {
public:
    SphereMap spheres;
    FaceMap faces;

    inline void clear() {
        spheres.clear();
        faces.clear();
        oct_.clear();
    }

    /**
     * @brief Enable octree for finding neighbours
     *
     * Box origin and dimension must be chosen so that all spheres lie entirely
     * inside the box (not just their centres).
     *
     * @param x0 box origin
     * @param l box dimension
     */
    void set_octree(const Vec3d& x0, double l) {
        use_oct_ = true;
        oct_.set_box(x0, l);
    }

    /**
     * @brief Disable octree insertion
     */
    void unset_octree() {
        use_oct_ = false;
        oct_.clear();
    }

    bool uses_octree() const { return use_oct_; }

    auto& octree() { return oct_; }

    bool insert_sphere(Sphere s);
    bool insert_sphere(int id, Vec3d r, double R) {
        return insert_sphere(Sphere(id, r, R));
    }
    FullGeo calculate_NVAS();
    void derive(Jacobians& J, double h = 1e-3);
    IdSet neighbours(int i) const;
    void enumerate();
    auto& interface(int i, int j) { return faces.at(Face::get_id(i, j)); }

#ifndef NDEBUG
    Face& dbg_interface(int i, int j);
    Sphere& dbg_sphere(int i);
#endif

private:
    void delete_sphere(int id);
    void delete_face(int id);
    SphereMap local_cluster(int i) const;
    IdMap face_row_map(int i) const;

    bool use_oct_ = false;
    Octree<IdVec> oct_;
};
} // namespace fusedspheres
