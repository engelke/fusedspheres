#pragma once
#include "arcpoly.hpp"

namespace fusedspheres {
struct Face : ArcPoly {
    static int get_id(int i, int j) { return (i + j) * (i + j + 1) / 2 + j; }
    Face() {}
    Face(int i, int j) : id(get_id(i, j)), s{i, j} {}

    int id = -1;  //< identifier (cantor enumeration of sphere ids)
    int row = -1; //< row index
    std::array<int, 2> s{-1, -1};      //< sphere ids
    std::array<double, 2> v{NAN, NAN}; //< distance to center
    Vec3d n;                           //< normal pointing from s[0] to s[1]
    Vec3d r;                           //< center
    Eigen::Matrix<double, 2, 3> P;     //< projection matrix

    double dist() const { return v[0] + v[1]; }
    double omega(size_t i) const;
    bool truncate(Vec3d r1, Vec3d n1);
};
} // namespace fusedspheres
