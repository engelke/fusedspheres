#pragma once

#include "Eigen/Dense"
#include "Eigen/Sparse"
#include <concepts>

namespace fusedspheres {
using Vec2d = Eigen::Vector2d;
using Vec3d = Eigen::Vector3d;
using VecXd = Eigen::VectorXd;
using Mat23d = Eigen::Matrix<double, 2, 3>;
using Arr2d = Eigen::Array2d;
using Arr3d = Eigen::Array3d;
using ArrXd = Eigen::ArrayXd;
/* Enable OpenMP for Eigen::BiCGSTAB solve
 * https://eigen.tuxfamily.org/dox/TopicMultiThreading.html*/
using SpMat = Eigen::SparseMatrix<double, Eigen::RowMajor>;

constexpr double eps = 1e-13;

template <typename T>
concept EigenMatrix = std::is_base_of_v<Eigen::MatrixBase<T>, T>;

inline auto cantor(auto i, auto j) { return (i + j) * (i + j + 1) / 2 + j; }

/**
 * @brief Get sign of val
 *
 * @param val comparable signed number
 * @return +1 or -1 int
 */
inline int sign(auto val) {
    return (decltype(val)(0) < val) - (val < decltype(val)(0));
}

/**
 * @brief Check if two eigen objects are identical
 */
inline bool is_identical(EigenMatrix auto const& a, EigenMatrix auto const& b) {
    return ((a - b).array().abs() < eps).all();
}

/**
 * @brief Calculate the closest approach of line to point p
 *
 * @param x support vector
 * @param d direction vector
 * @param p point
 * @return distance
 */
auto distance_point_line(EigenMatrix auto const& x, EigenMatrix auto const& d,
                         EigenMatrix auto p) {
    p -= x;
    return (p - p.dot(d) / d.squaredNorm() * d).norm();
}

/**
 * @brief Calculate the closest approach of line to point p
 *
 * @param l1 first point on line
 * @param l2 second point on line
 * @param p point
 * @return distance
 */
auto distance_point_line_through(EigenMatrix auto const& l1,
                                 EigenMatrix auto const& l2,
                                 EigenMatrix auto const& p) {
    return distance_point_line(l1, l2 - l1, p);
}

/**
 * @brief Calculate the point of intersection of two lines
 *
 * @param a1 first point on line 1
 * @param a2 second point on line 1
 * @param b1 first point on line 2
 * @param b2 second point line 2
 * @return point of intersection
 */
inline Vec2d line_intersection(Vec2d const& a1, Vec2d const& a2,
                               Vec2d const& b1, Vec2d const& b2) {
    using Line2d = Eigen::Hyperplane<double, 2>;
    Line2d l1 = Line2d::Through(a1, a2);
    Line2d l2 = Line2d::Through(b1, b2);
    return l1.intersection(l2);
}

inline double cross2(Vec2d const& a, Vec2d const& b) {
    return a[0] * b[1] - a[1] * b[0];
}

/*!
 * @brief Computes the solid angle of a lune.
 *
 * The solid angle is equivalent to the surface area on the unit sphere.
 * The region on the surface is delimited by two planes. One plane is a
 * geodesic whereas the other has a perpendicular distance $ v $
 * to the center of the unit sphere. The arc length of the non-geodesic part
 * with respect to the center of the disk is denoted $ \phi $
 *
 * @param phi arc angle (domain: [0, 2 * pi) )
 * @param v distance of the non-geodesic plane to the center of the unit
 * sphere (domain: [0,  1)
 * @return solid angle [0, 2 * pi)
 */
inline double solid_angle_lune(const double phi, const double v) {
    // double sin_phi = sin(phi);
    // double cos_phi = cos(phi);

    // double a = v * (1 - cos_phi);
    // double s = sign(phi - M_PI);

    // return M_PI - phi * v + 2 * s * acos(a / sqrt(sin_phi * sin_phi + a *
    // a));
    return M_PI - phi * v - 2 * atan2(sin(phi), v * (1 - cos(phi)));
}

/*!
 * @brief Compute the solid angle of tetrahedron with corners {O, a, b, c}
 * @param  a Corner 1
 * @param  b Corner 2
 * @param  c Corner 3
 * @return   Solid angle
 */
inline double solid_angle_tetrahedron(const Vec3d& a, const Vec3d& b,
                                      const Vec3d& c) {
    /* a, b, c are normalized */
    double y = a.dot(b.cross(c));
    double x = 1 + b.dot(c) + c.dot(a) + a.dot(b);

    return 2 * atan2(y, x);
}

/**
 * @brief Create a right handed projection onto a plan given by its normal
 *
 * @param z normal of plane
 * @return projection matrix 2x3
 */
inline Mat23d generate_projection(Vec3d z) {
    /* get most prominent world direction, call it z */
    Eigen::Index i;
    z.maxCoeff(&i);

    /* get the y direction of world */
    i = (i + 2) % 3;
    Vec3d ey = Vec3d::Unit(i);

    Mat23d P;
    P.row(0) = ey.cross(z).normalized();
    P.row(1) = z.cross(P.row(0));

    return P;
}
} // namespace fusedspheres
