#pragma once
#include <algorithm>
#include <iterator>

namespace fusedspheres {
template <class Iterable>
Iterable set_union(const Iterable& set1, const Iterable& set2) {
    Iterable result;
    std::set_union(set1.begin(), set1.end(), set2.begin(), set2.end(),
                   std::inserter(result, result.begin()));
    return result;
}

template <class Iterable>
Iterable set_intersection(const Iterable& set1, const Iterable& set2) {
    Iterable result;
    std::set_intersection(set1.begin(), set1.end(), set2.begin(), set2.end(),
                          std::inserter(result, result.begin()));
    return result;
}

template <class Iterable>
Iterable set_difference(const Iterable& set1, const Iterable& set2) {
    Iterable result;
    std::set_difference(set1.begin(), set1.end(), set2.begin(), set2.end(),
                        std::inserter(result, result.begin()));
    return result;
}

template <class Map, class Keys> Map select(const Map& map, const Keys& keys) {
    Map selection;
    for (auto k : keys) {
        selection.insert({k, map.at(k)});
    }
    return selection;
}

/**
 * Replace elements [i1, i2) with [el1, el2], wrap cyclically if i1 > i2 */
template <class Iterable, class Element = typename Iterable::value_type>
void splice(Iterable& c, int i1, int i2, Element el1, Element el2) {
    auto it1 = c.begin() + i1;
    auto it2 = c.begin() + i2;

    /* delete and insert between i1 and i2 (> i1)
     * before  0    1    2    3    4    end
     *              ^i1            ^i2
     * after   0    el1  el2  4    end */
    if (i1 <= i2) {
        /* iterator to element with previous (before erase) position i2 */
        it2 = c.erase(it1, it2);
        c.insert(it2, {el1, el2});
    }
    /* delete and insert between i1 and i2 (< i1) (periodicity!)
     * before  0    1    2    3    4    end
     *              ^i2       ^i1
     * after   el1  el2  1    2    end */
    else if (i1 > i2) {
        c.erase(it1, c.end());
        c.erase(c.begin(), it2);
        c.push_back(el1);
        c.push_back(el2);
    }

    // it2 = c.insert(it2, el2);
}

template <typename Container>
auto cyclic_erase_between(Container& c, typename Container::iterator first,
                          typename Container::iterator last) requires
    std::forward_iterator<typename Container::iterator> {
    c.splice_after(c.before_begin(), c, first, c.end());

    while (c.begin() != last) {
        c.erase_after(c.before_begin());
    }

    return std::pair{first, c.begin()};
}

template <typename Container>
auto cyclic_next(Container& c, typename Container::iterator it) {
    ++it;
    if (it == c.end())
        it = c.begin();

    return it;
}

template <typename Container>
auto cyclic_next(const Container& c, typename Container::const_iterator it) {
    ++it;
    if (it == c.end())
        it = c.begin();

    return it;
}
} // namespace fusedspheres
