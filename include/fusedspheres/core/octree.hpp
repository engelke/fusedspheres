#pragma once
#include "linalg.hpp"

#include <array>
#include <concepts>
#include <deque>
#include <memory>
#include <ranges>
#include <stdexcept>

namespace fusedspheres::octree {
/**
 * @brief Return the index of the child node
 *
 * @param x position in node coordinates, 0 <= x[i] < 1
 * @return index in [0, 8)
 */
inline int to_index(const Arr3d& x) {
    Eigen::Array3i a = (2 * x).cast<int>();
    if ((a < 0).any() || (a >= 2).any())
        throw std::out_of_range("position not contained in current cell");
    return 4 * a[0] + 2 * a[1] + a[2];
}

/**
 * @brief Transform relative coordinate in node to relative coordinate in child
 * node
 *
 * @param x position in node coordinates, 0 <= x[i] < 1
 * @return child node coordinates, 0 <= x[i] < 1
 */
inline Arr3d to_relative(Arr3d x) {
    x *= 2;
    x -= x.floor();
    return x;
}

template <typename ValueType> class Octree {
public:
    class Node {
    public:
        using UPtr = std::unique_ptr<Node>;
        using ValueUPtr = std::unique_ptr<ValueType>;

        Node(Node* parent) : parent_(parent) {}

        /**
         * @brief Find the deepest existing node containing a sphere at position
         * x and radius R.
         *
         * @param x sphere centre
         * @param R sphere radius
         * @return pointer to node
         */
        Node* find(Arr3d x, double R) {
            if (R <= 0)
                throw std::domain_error("R must be greater than zero");

            auto i = to_index(x - R);
            auto j = to_index(x + R);

            /* both extreme points lie in the same octant and child exists =>
             * continue */
            if (i == j && children_[i])
                return children_[i]->find(to_relative(x), 2 * R);
            /* extreme points lie in different octants => this is the deepest
             * node */
            else
                return this;
        }

        /**
         * @brief Find the deepest existing node containing the point x
         *
         * @param x point
         * @return pointer to node
         */
        Node* find(Arr3d x) {
            auto i = to_index(x);
            if (children_[i])
                return children_[i]->find(to_relative(x));
            else
                return this;
        }

        /**
         * @brief Get a pointer to the node containing the sphere at position x
         * and radius, insert missing nodes on the way.
         *
         * @param x sphere centre in node coordinates, 0 <= x[i] < 1
         * @param R sphere radius R > 0
         * @return pointer to node
         */
        Node* insert(Arr3d x, double R) {
            if (R <= 0)
                throw std::domain_error("R must be greater than zero");

            auto i = to_index(x - R);
            auto j = to_index(x + R);

            /* both extreme points lie in the same octant => continue */
            if (i == j) {
                /* insert missing child */
                if (!children_[i])
                    children_[i] = std::make_unique<Node>(this);

                return children_[i]->insert(to_relative(x), 2 * R);
            }

            return this;
        }

        constexpr bool has_value() const { return value_ptr_ != nullptr; }

        constexpr const auto& value_ptr() const { return value_ptr_; }
        constexpr auto& value_ptr() { return value_ptr_; }

        constexpr const auto& value() const { return *value_ptr_; }
        constexpr auto& value() { return *value_ptr_; }

        constexpr const auto parent() const { return parent_; }
        constexpr auto parent() { return parent_; }

        constexpr const auto& children() const { return children_; }
        constexpr auto& children() { return children_; }

        // auto children() const {
        //     return children_ | std::views::filter([](const auto& ptr) {
        //                return ptr != nullptr;
        //            }) |
        //            std::views::transform([](const auto& ptr) -> const Node* {
        //                return ptr.get();
        //            });
        // }
        // auto children() {
        //     return children_ | std::views::filter([](const auto& ptr) {
        //                return ptr != nullptr;
        //            }) |
        //            std::views::transform(
        //                [](auto& ptr) -> Node* { return ptr.get(); });
        // }

    private:
        Node* parent_;
        std::array<UPtr, 8> children_;
        ValueUPtr value_ptr_;
    };

    struct ValueRange {
        struct Sentinel {};
        class Iterator {
        public:
            using value_type = ValueType;

            Iterator(Node* start) {
                /* insert start */
                stack_.emplace_back(start, true);

                /* prepend all parent nodes marked as no-descend */
                while ((start = start->parent()) != nullptr) {
                    stack_.emplace_front(start, false);
                }

                if (!stack_.front().first->has_value())
                    go_to_next_value();
            }

            /**
             * @brief Compare to Sentinel, evaluates to true if no nodes remain
             * on the stack
             */
            bool operator==(const Sentinel&) const {
                return stack_.size() == 0;
            }

            /**
             * @brief Continue to
             *
             * @return Iterator&
             */
            Iterator& operator++() {
                if (stack_.size() > 0)
                    go_to_next_value();

                return *this;
            }

            /**
             * @brief Dereference
             *
             * @return ValueType&
             */
            ValueType& operator*() { return stack_.front().first->value(); }

        private:
            std::deque<std::pair<Node*, bool>> stack_;

            void go_to_next_value() {
                /* traverse tree until finding a new value or stack is empty */
                do {
                    /* pop the first element off the stack */
                    auto [node, descend] = stack_.front();
                    stack_.pop_front();
                    /* if node is marked for descending */
                    if (descend) {
                        /* add all its children to the stack */
                        for (auto& child : node->children())
                            if (child)
                                stack_.emplace_back(child.get(), true);
                    }
                } while (stack_.size() > 0 &&
                         !stack_.front().first->has_value());
            }
        };

        ValueRange(Node* start) : start_(start) {}

        Iterator begin() { return Iterator(start_); }
        Sentinel end() { return Sentinel(); }

        Node* start_;
    };

    /**
     * @brief Default initialise with origin at {0,0,0} and box length of 1
     */
    Octree() : x0_{0, 0, 0}, l_(1), root_(std::make_unique<Node>(nullptr)) {}

    /**
     * @brief Construct Octree given the box origin and length
     *
     * @param x0 box origin
     * @param l box length
     */
    Octree(const Arr3d x0, double l)
        : x0_(x0), l_(l), root_(std::make_unique<Node>(nullptr)) {}

    /**
     * @brief Find the node containing the sphere at position x and
     * radius R and return a reference to the associated value.
     * Missing nodes will be inserted. If the value does not exist, it
     * will be default initialised.
     *
     * @param x sphere centre
     * @param R sphere radius
     * @return reference to associated value
     */
    auto& get(Arr3d x,
              double R) requires std::default_initializable<ValueType> {
        x = (x - x0_) / l_;
        R /= l_;

        auto node = root_->insert(x, R);

        if (!node->value_ptr())
            node->value_ptr() = std::make_unique<ValueType>();

        return *node->value_ptr();
    }

    /**
     * @brief Insert a new value at the node containing the sphere at position x
     * and radius R. Existing values will be overwritten.
     *
     * @param x sphere centre
     * @param R sphere radius
     * @param value
     */
    void insert(Arr3d x, double R, const ValueType& value) {
        x = (x - x0_) / l_;
        R /= l_;

        auto node = root_->insert(x, R);
        node->value_ptr() = std::make_unique<ValueType>(value);
    }

    /**
     * @brief Insert a new value at the node containing the sphere at position x
     * and radius R. Existing values will be overwritten.
     *
     * @param x sphere centre
     * @param R sphere radius
     * @param value
     */
    void insert(Arr3d x, double R, ValueType&& value) {
        x = (x - x0_) / l_;
        R /= l_;

        auto node = root_->insert(x, R);
        node->value_ptr() = std::make_unique<ValueType>(std::move(value));
    }

    /**
     * @brief Return an iterable range of all values at nodes that are parents
     * or children of the node containing a sphere at position x with radius R.
     *
     * @param x sphere centre
     * @param R sphere radius
     * @return ValueRange object
     */
    auto traverse(Arr3d x, double R) {
        x = (x - x0_) / l_;
        R /= l_;

        auto start = root_->find(x, R);
        return ValueRange(start);
    }

    /**
     * @brief Return an iterable range of all values at existing nodes that
     * contain the point x.
     *
     * @param x point
     * @return ValueRange object
     */
    auto traverse(Arr3d x) {
        x = (x - x0_) / l_;

        auto start = root_->find(x);
        return ValueRange(start);
    }

    constexpr auto& root() { return root_; }
    constexpr const auto& root() const { return root_; }

    /**
     * @brief Remove all entries
     */
    void clear() { root_ = std::make_unique<Node>(nullptr); }

    /**
     * @brief Remove all entries and change the box size
     *
     * @param x0 box origin (minimum coordinate)
     * @param l box length
     */
    void set_box(const Arr3d& x0, double l) {
        clear();
        x0_ = x0;
        l_ = l;
    }

private:
    Arr3d x0_; //< origin of box
    double l_; //< box dimension
    std::unique_ptr<Node> root_;
};
} // namespace fusedspheres::octree