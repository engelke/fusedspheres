#include "../extern/parallel_hashmap/phmap.h"
// #include <unordered_map>
// #include <set>

namespace fusedspheres {
template <typename T1, typename T2>
using Map = phmap::parallel_flat_hash_map<T1, T2>;
// using Map = std::unordered_map<T1, T2>;

template <typename T> using Set = phmap::flat_hash_set<T>;
// template <typename T> using Set = std::set<T>;
} // namespace fusedspheres