#pragma once
#include "linalg.hpp"
#include "map.hpp"

namespace fusedspheres {
struct Sphere {
    inline Sphere() {}
    explicit inline Sphere(int p_id, Vec3d p_r, double p_R, int p_row = -1)
        : id(p_id), row(p_row), r(p_r), R(p_R) {}
    int id;         //< identifier
    int row = -1;   //< row index
    Vec3d r;        //< center position
    double R;       //< radius
    Set<int> faces; //< set of face indices
};
} // namespace fusedspheres
