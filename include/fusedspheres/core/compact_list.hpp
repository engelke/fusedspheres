#include <type_traits>
#include <vector>

namespace fusedspheres {

/**
 * @brief Forward linked list based on a compact memory layout
 *
 * @tparam T value type
 */
template <typename T> class CompactList {
public:
    using Index = long;
    static constexpr Index END = -1;

    struct Node {
        Node(const T& v, Index n) : value(v), next(n) {}
        Node(T&& v, Index n) : value(std::move(v)), next(n) {}

        T value;
        Index next = END;
    };
    using NodeVector = std::vector<Node>;

    // struct EndSentinel {};
    struct BeforeBeginSentinel {};
    template <bool Const> struct Iterator {
        using iterator_category = std::forward_iterator_tag;
        using value_type = T;
        using reference = std::conditional_t<Const, const T&, T&>;
        using pointer = std::conditional_t<Const, const T*, T*>;
        using difference_type = Index;

        using Data = std::conditional_t<Const, const NodeVector, NodeVector>;

        Data* data = nullptr;
        Index pos = END;

        Iterator() {}
        Iterator(Data& d, Index p) : data(&d), pos(p) {}
        Iterator(const Iterator&) = default;
        Iterator(Iterator&&) = default;
        Iterator& operator=(const Iterator&) = default;
        Iterator& operator=(Iterator&&) = default;

        Iterator& operator++() {
            pos = (*data)[pos].next;
            return *this;
        }

        Iterator operator++(int) {
            auto it = *this;
            operator++();
            return it;
        }
        reference operator*() { return (*data)[pos].value; }
        pointer operator->() { return &(*data)[pos].value; }

        friend bool operator==(const Iterator& a, const Iterator& b) {
            return a.pos == b.pos;
        }
        // bool operator==(const EndSentinel&) { return pos == END; }
    };

    using iterator = Iterator<false>;
    using const_iterator = Iterator<true>;

    CompactList() {}

    /**
     * @brief Construct a new CompactList from an initializer_list
     *
     * @param values
     */
    CompactList(std::initializer_list<T> values) {
        data_.reserve(values.size());
        Index cnt = 0;
        for (auto it = std::begin(values); it != std::end(values); ++it) {
            data_.emplace_back(*it, ++cnt);
        }
        head_ = 0;
        data_.back().next = END;
    }

    iterator begin() { return iterator(data_, head_); }
    const_iterator begin() const { return const_iterator(data_, head_); }
    const_iterator cbegin() const { return const_iterator(data_, head_); }

    iterator end() { return iterator(data_, END); }
    const_iterator end() const { return const_iterator(data_, END); }
    const_iterator cend() const { return const_iterator(data_, END); }

    BeforeBeginSentinel before_begin() { return BeforeBeginSentinel(); }

    /**
     * @brief Delete all elements in the container.
     *
     * @warning No destructors are called!
     */
    void clear() {
        auto last = search_before(head_, END);
        data_[last].next = empty_;
        empty_ = head_;
        head_ = END;
    }

    /**
     * @brief Returns true if no elements are stored
     */
    bool empty() const { return head_ == END; }

    /**
     * @brief Insert a new element after iterator
     *
     * @param it position of element before new element
     * @param value value to be inserted
     * @return iterator to new value
     */
    auto insert_after(iterator it, const T& value) {
        auto pos = insert_value(value);
        auto& n1 = data_[it.pos];
        auto& n2 = data_[pos];
        n2.next = n1.next;
        n1.next = pos;
        return iterator(data_, pos);
    }

    /**
     * @brief Insert a new  element after position
     *
     * @param value
     * @return iterator to new value
     */
    auto insert_after(BeforeBeginSentinel, const T& value) {
        auto pos = insert_value(value);
        auto& n = data_[pos];
        n.next = head_;
        head_ = pos;
        return iterator(data_, pos);
    }

    /**
     * @brief Add a new element at the beginning
     *
     * @param value
     */
    void push_front(const T& value) {
        auto pos = insert_value(value);
        auto& n = data_[pos];
        n.next = head_;
        head_ = pos;
    }

    /**
     * @brief Add a new element at the beginning
     *
     * @param value prvalue or xvalue to be inserted
     */
    void push_front(T&& value) {
        auto pos = insert_value(std::move(value));
        auto& n = data_[pos];
        n.next = head_;
        head_ = pos;
    }

    /**
     * @brief Delete elements in range (first, last). If last <= first the
     * behaviour is undefined.
     *
     * @warning No destructors are called!
     *
     * @param first element before first deleted element
     * @param last element after last deleted element
     * @return last
     */
    auto erase_between(BeforeBeginSentinel, iterator last) {
        auto last_deleted = search_before(head_, last.pos);
        data_[last_deleted].next = empty_;
        empty_ = head_;
        head_ = last.pos;

        return last;
    }

    /**
     * @brief Delete elements in range (first, last). If last <= first the
     * behaviour is undefined.
     *
     * @warning No destructors are called!
     *
     * @param first element before first deleted element
     * @param last element after last deleted element
     * @return last
     */
    auto erase_between(iterator first, iterator last) {
        auto last_deleted = search_before(first.pos, last.pos);
        data_[last_deleted].next = empty_;
        empty_ = data_[first.pos].next;
        data_[first.pos].next = last.pos;

        return last;
    }

    /**
     * @brief Erase all elements in the range (first, last) treating the list as
     * a cyclic buffer. Resulting list will start at last and end at first.
     *
     * @warning No destructors are called!
     *
     * @param first element before first deleted element
     * @param last element after last deleted element
     * @return auto
     */
    void cyclic_erase_between(iterator first, iterator last) {
        /* connect elements cyclically and find last element before last */
        auto i = first.pos;
        auto before_last = END;
        do {
            if (data_[i].next == END)
                data_[i].next = head_;

            if (data_[i].next == last.pos)
                before_last = i;

            i = data_[i].next;
        } while (i != first.pos);

        /* move deleted element to empty stack */
        data_[before_last].next = empty_;
        empty_ = data_[first.pos].next;
        /* new list goes from last to it */
        data_[first.pos].next = END;
        head_ = last.pos;
    }

    /**
     * @brief Reserve storage for n elements
     *
     * @param n number of elements
     */
    void reserve(Index n) { data_.reserve(n); }

private:
    std::vector<Node> data_; //< data array
    Index head_ = END;       //< start of occupied nodes
    Index empty_ = END;      //< start of empty nodes

    /**
     * @brief Copy value to a new or recycled node
     *
     * @param value value to be inserted
     * @return index on new node
     */
    Index insert_value(const T& value) {
        if (empty_ == END) {
            data_.emplace_back(value, END);
            return data_.size() - 1;
        }

        auto& node = data_[empty_];
        node.value = value;
        auto pos = empty_;
        empty_ = node.next;
        return pos;
    }

    /**
     * @brief Move value into a new or recycled node
     *
     * @param value prvalue or xvalue to be inserted
     * @return index of new node
     */
    Index insert_value(T&& value) {
        if (empty_ == END) {
            data_.emplace_back(std::move(value), END);
            return data_.size() - 1;
        }

        auto& node = data_[empty_];
        node.value = std::move(value);
        auto pos = empty_;
        empty_ = node.next;
        return pos;
    }

    /**
     * @brief Find last node before next.
     *
     * @param start start of iteration
     * @param next position after node
     * @return position
     */
    Index search_before(Index start, Index next) {
        while (data_[start].next != next && data_[start].next != END)
            start = data_[start].next;

        return start;
    }
};
} // namespace fusedspheres