#pragma once

#include "algorithm.hpp"
#include "compact_list.hpp"
#include "linalg.hpp"

#include <concepts>
#include <iterator>
#include <optional>

namespace fusedspheres {
namespace detail {
    // /**
    //  * @brief Find entry and exit segment of truncation line
    //  *
    //  * @param is_member bools (start point of seg not truncated)
    //  * @return pair of entry and exit index
    //  */
    // auto find_intersections(const auto& is_member) {
    //     int n = is_member.size();
    //     int i1 = n, i2 = n;
    //     /* find the first intersection with the arcpolygon along the line
    //      * (d, u)
    //      */
    //     for (int i = 0; i < n; ++i) {
    //         /* for any segment of the arcpolygon there is an intersection if
    //          * we pass between two points */
    //         i1 = (is_member[i] && !is_member[(i + 1) % n]) ? i : i1;
    //         i2 = (!is_member[i] && is_member[(i + 1) % n]) ? i : i2;
    //     }

    //     return std::make_pair(i1, i2);
    // }

    // /**
    //  * @brief Find the arc segment on which p lies
    //  *
    //  * @param segments iterable of segments
    //  * @param p point on the circumference of the arc polygon
    //  * @return index of segment
    //  */
    // auto find_arc_intersection(const auto& segments, const Vec2d& p) {
    //     size_t i = 0;
    //     for (; i < segments.size(); ++i) {
    //         auto& seg = segments[i];
    //         auto& next = segments[(i + 1) % segments.size()];
    //         if (seg.is_arc() && cross2(p - seg.p, next.p - seg.p) > 0)
    //             break;
    //     }
    //     return i;
    // }

    /**
     * @brief Check if point x is member of the halfspace defined by the line
     * through point a in direction d.
     *
     * @param a support vector of line
     * @param d direction vector of line
     * @param x point to check
     * @return true point x is member of halfspace
     * @return false point x is not member of halfspace
     */
    inline bool point_is_left_of(const Vec2d& a, const Vec2d& d, Vec2d x) {
        x -= a;
        return cross2(d, x) > 0;
    }

    enum class APTrunc {
        Regular,  // Line passes between corners
        Empty,    // Region intersection is empty
        None,     // No truncation
        CircSeg,  // Result is a circular segment
        ArcTrunc, // One arc is truncated
    };

    /**
     * @brief Find the segments in [first, last) that are intersected by the
     * line through a and b.
     *
     * @tparam It segment iterator type
     * @param a first point (on circle)
     * @param b second point (on circle)
     * @return tuple of APTrunc enum and iterators to entry and exit segment
     */
    template <typename Container, typename It = typename Container::iterator>
    auto find_intersected_segments(Container& segments, const Vec2d& a,
                                   const Vec2d b)
        -> std::tuple<APTrunc, It, It> {
        It in, out;
        // int i_in, i_out;
        Vec2d d = b - a;

        auto curr = segments.begin();
        auto l_curr = point_is_left_of(a, d, curr->p);
        bool all_left = true;
        bool any_left = false;
        int cnt = 0;

        // TODO end iteration if in and out have been found
        while (curr != segments.end()) {
            auto next = cyclic_next(segments, curr);
            auto l_next = point_is_left_of(a, d, next->p);

            all_left &= l_curr;
            any_left |= l_curr;

            if (l_curr && !l_next) { // found regular entry
                in = curr;
                // i_in = cnt;
            } else if (!l_curr && l_next) { // found regular exit
                out = curr;
                // i_out = cnt;
            } else if (curr->is_arc()) { // check arc intersection
                /* only one point needs to be checked for arcs */
                auto l_new = point_is_left_of(curr->p, next->p - curr->p, a);

                if (l_curr && !l_new) {
                    return {APTrunc::ArcTrunc, curr, curr};
                } else if (!l_curr && !l_new) {
                    return {APTrunc::CircSeg, curr, curr};
                }
                // else if (!l_curr && l_new) {
                //     return {APTrunc::Empty, curr, curr};
                // }
                /* continue iteration if m_curr && !m_new */
            }
            /* go to next segment */
            curr = std::next(curr);
            l_curr = l_next;
            ++cnt;
        }

        /* line does not intersect any segment */
        if (all_left) {
            return {APTrunc::None, segments.end(), segments.end()};
        } else if (!any_left) {
            return {APTrunc::Empty, segments.end(), segments.end()};
        }

        /* generic case of intersection */
        return {APTrunc::Regular, in, out};
    }
} // namespace detail

struct Seg {
    Vec2d p;    //< start point
    Vec2d d;    //< direction vector
    double u;   //< offset from center
    double phi; //< arc length for arc, negative for line

    bool is_line() const { return phi < 0; }
    bool is_arc() const { return !is_line(); }
};

struct ArcPoly {
    double R;          //< radius
    double area = NAN; //<
    CompactList<Seg> segments;

    /**
     * @brief Truncate arc-polygon by a half-plane defined by its normal vector
     * and offset from origin. The normal points into the truncated space.
     *
     * @param n normal
     * @param u offset from origin
     * @return true if remaining region is not empty
     * @return false remaining region is empty
     */
    bool truncate(Vec2d n, double u);

    /**
     * @brief Populate the angle variables of the segments and area
     */
    void populate();

    /**
     * @brief Count lines and corners
     */
    std::pair<int, int> num_lines_corners() const;

    /**
     * @brief Calculate the total arc length of the arc-polygon
     */
    double arc_length() const;

    /**
     * @brief Calculate the circumference of the arc-polygon
     *
     * @return double
     */
    double circumference() const;
};

} // namespace fusedspheres
